#pragma once

#ifdef UNICODE
	#undef UNICODE
	#include "tinyxml.h"
	#define UNICODE
#else
	#include "tinyxml.h"
#endif

#include <DBBaseClass_SQLApi.h>
#include <SQLAPI.h>
#include <list>
#include <vector>

class PLANTHEADER
{
public:
	PLANTHEADER() { Reset(); }
	~PLANTHEADER() {}

	int tradslag;
	int vitalitet;
	int orsak;				// Orsak (om d�lig vitalitet)
	int planteringspunkt;
	int battre;				// Finns b�ttre (J/N)
	int planteringsdjup;	// Planteringsdjup
	int tilltryckning;

	void Reset()
	{
		tradslag			= -1;
		vitalitet			= -1;
		orsak				= -1;
		planteringspunkt	= -1;
		battre				= -1;
		planteringsdjup		= -1;
		tilltryckning		= -1;
	}
};

typedef std::vector<PLANTHEADER> PlantVector;


class PLOTHEADER
{
public:
	PLOTHEADER() { Reset(); }
	~PLOTHEADER() {}

	// Nyckelvariabler
	CString region;
	CString distrikt;
	CString lag;
	CString karta;

	CString koordinatX;
	CString koordinatY;
	int grundfor;			// Grundf�rh�llande
	int ytstruktur;
	int lutning;
	int vegTyp;
	int jordart;
	int markfukt;
	int uppfr;				// Uppfrysningsmark (J/N)
	CString si;				// St�ndortsindex
	int tradslag;			// Tr�dslag (tall/gran)
	int markberedning;		// Markberedning (markberedning)
	int orsak;				// Orsak (om ej markberett)
	int svarighet;			// Sv�righetsgrad MB
	int antalSpar;			// Antal sp�r
	int sparBredd;			// Sp�rbredd
	int antalFlackar;		// Antal fl�ckar
	float arealPerFlack;	// Areal/fl�ck
	int vars[9];			// Markberedningspunkt (CYtaPage7-8)
	int sfpBarr;			// Sj�lvf�ryngrande plantor (barr), endast plantinv
	int sfpLov;				// Sj�lvf�ryngrande plantor (l�v),  endast plantinv
	int rattTradslag;		// R�tt tr�dslag planterat, endast plantinv
	int rattMbMetod;		// R�tt mb metod

	int ytnr;				// Unik identifierare
	int lopnr;				// R�knare per trakt

	PlantVector plants;

	void Reset()
	{
		region.Empty();
		distrikt.Empty();
		lag.Empty();
		karta.Empty();
		koordinatX.Empty();
		koordinatY.Empty();
		grundfor		= -1;
		ytstruktur		= -1;
		lutning			= -1;
		vegTyp			= -1;
		jordart			= -1;
		markfukt		= -1;
		uppfr			= -1;
		si.Empty();
		tradslag		= -1;
		markberedning	= -1;
		orsak			= -1;
		svarighet		= -1;
		antalSpar		= -1;
		sparBredd		= -1;
		antalFlackar	= -1;
		arealPerFlack	= -1;
		memset(vars, 0, sizeof(vars));
		sfpBarr			= -1;
		sfpLov			= -1;
		rattTradslag	= -1;
		rattMbMetod		= -1;
		ytnr			= -1;
		lopnr			= -1;
	}
};

typedef std::vector<PLOTHEADER> PlotVector;


struct TRACTHEADER
{
	TRACTHEADER() { Reset(); }
	~TRACTHEADER() {}

	bool slutford;		// Slutf�rd (intern flagga)

	int uppfoljning;	// Markberedning/plantinventering
	int invTyp;			// Inventeringstyp (egen, disrikts, etc.)
	int ursprung;		// Ursprung (egen skog, rotk�p, etc.)
	CString region;
	CString distrikt;
	CString lag;
	CString user;
	CString datum;
	CString karta;
	CString bestand;
	int delbestand;
	CString namn;		// Trakt/best�ndsnamn
	float areal;
	int hoh;
	float radie;
	int grundfor;		// Grundf�rh�llande
	int ytstruktur;
	int lutning;
	int vegTyp;
	int jordart;
	int markfukt;
	int uppfr;			// Uppfrysningmark (J/N)
	CString si;			// St�ndortsindex
	int svarighet;		// Sv�righetsgrad MB
	int myr;
	int hallmark;		// H�llmark
	int omrSkyddszon;	// Omr�de med skyddszon
	int skots;			// Object som sk�ts
	int fornminnen;
	int kultur;			// Kulturl�mning
	int skyddszoner;
	int naturvarde;		// Naturv�rdes/d�da tr�d
	int skador;			// Skador p� mark och vatten
	CString koordinatX;
	CString koordinatY;
	int markpaverkan;	// Markp�verkan
	int nedskrapning;	// Nedskr�pning
	CString text;		// Fri text

	PlotVector plots;

	void Reset()
	{
		slutford		= false;
		uppfoljning		= -1;
		invTyp			= -1;
		ursprung		= -1;
		region.Empty();
		distrikt.Empty();
		lag.Empty();
		user.Empty();
		datum.Empty();
		karta.Empty();
		bestand.Empty();
		delbestand		= -1;
		namn.Empty();
		areal			= -1;
		hoh				= -1;
		radie			= 0.0f;
		grundfor		= -1;
		ytstruktur		= -1;
		lutning			= -1;
		vegTyp			= -1;
		jordart			= -1;
		markfukt		= -1;
		uppfr			= -1;
		si.Empty();
		svarighet		= -1;
		myr				= -1;
		hallmark		= -1;
		omrSkyddszon	= -1;
		skots			= -1;
		fornminnen		= -1;
		kultur			= -1;
		skyddszoner		= -1;
		naturvarde		= -1;
		skador			= -1;
		koordinatX.Empty();
		koordinatY.Empty();
		markpaverkan	= -1;
		nedskrapning	= -1;
		text.Empty();
	}
};

typedef std::vector<TRACTHEADER> TractVector;


struct TEAMHEADER
{
	TEAMHEADER() { Reset(); }
	~TEAMHEADER() {}

	int region;
	int distrikt;
	int lagnr;
	CString lagnamn;

	void Reset()
	{
		region		= -1;
		distrikt	= -1;
		lagnr		= -1;
		lagnamn.Empty();
	}
};

typedef std::vector<TEAMHEADER> TeamVector;


typedef std::list<int> IdList;


class CDBFunc : public CDBBaseClass_SQLApi
{
public:
	CDBFunc( DB_CONNECTION_DATA &condata );
	~CDBFunc();

	bool AddRecord(TRACTHEADER &th);
	bool AddTeam(TEAMHEADER &th);
	bool CalculateRecord(TRACTHEADER &th);
	bool ExportRecords(CString dbFile, CString conditions);
	bool DeleteTeam(TEAMHEADER &th);
	bool DeletePlot(PLOTHEADER &ph);
	bool DeleteTrakt(TRACTHEADER &th);
	bool GetTeamData(TeamVector &teams);
	bool GetPlotData(PlotVector &plots, int regionId = -1, int distriktId = -1, int lagId = -1, int karta = -1, int bestand = -1, int delbestand = -1);
	bool GetTraktData(TractVector &tracts);
	bool GetDistrictName(int districtId, int regionId, CString &districtName);
	bool GetRegionName(int regionId, CString &regionName);
	bool GetTeamName(int teamId, int districtId, int regionId, CString &teamName);
	bool GetDistrictList(int regionId, IdList &ids);
	bool GetTeamList(int districtId, int regionId, IdList &ids);
	bool GetRegionList(IdList &ids);
	bool MakeSureTablesExist();
	bool UpdateTeam(TEAMHEADER &th);
	bool UpdateTrakt(TRACTHEADER &th, int oldRegion, int oldDistrikt, int oldLag, int oldKarta, int oldBestand, int oldDelbestand);
	bool ShowImportDialog();

	const TRACTHEADER& GetLastTractKey() { return m_lastTractKey; }

	static void setupForDBConnection( HWND hWndReciv, HWND hWndSend );

protected:
	bool MoveFileToBackupDir(CString &filepath);

	bool ParseXml(CString &filename);
	bool ParseTrakt(TiXmlNode *pNode, CString &filename);
	bool ParseYta(TiXmlNode *pNode, TRACTHEADER &th, int plotnum);
	bool ParsePlanta(TiXmlNode *pNode, PLOTHEADER &ph);

	bool FormatDate(CString str, CString &date);

	TRACTHEADER m_lastTractKey; // Hold key values for last added tract (used to bring up last added record after import & reload is finished)
};
