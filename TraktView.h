#pragma once

#include "DBFunc.h"


class CTraktView : public CFormView
{
	DECLARE_DYNCREATE(CTraktView)

protected:
	CTraktView();
	virtual ~CTraktView();

	void BeginAdding();
	void EndAdding();
	void DeleteTrakt();
	void SaveTrakt();

	void EnableFields();
	void ListDistricts(int regionId);
	void ListTeams(int regionId, int districtId);
	void ReloadTraktData( bool bDataAdded = false );

	bool m_bAdding;
	CDBFunc *m_pDB;
	int m_nDBIndex;
	TractVector m_traktData;

public:
	void PopulateData(int idx = -1);
	void SetupToolbarButtons();

	virtual void OnInitialUpdate();

	enum { IDD = IDD_FORMTRAKT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg void OnCbnSelchangeDistrict();
	afx_msg void OnCbnSelchangeRegion();
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	DECLARE_MESSAGE_MAP()

	LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);

	CMyComboBox m_region;
	CMyComboBox m_distrikt;
	CMyComboBox m_lag;
	CMyExtEdit m_karta;
	CMyExtEdit m_areal;
	CMyComboBox m_uppfoljning;
	CMyComboBox m_invtyp;
	CMyComboBox m_ursprung;
	CMyExtEdit m_anvandare;
	CMyExtEdit m_hoh;
	CMyExtEdit m_bestand;
	CMyExtEdit m_delbestand;
	CMyExtEdit m_traktnamn;
	CDateTimeCtrl m_datum;
	CMyExtEdit m_radie;
	CMyComboBox m_grundforhallande;
	CMyComboBox m_jordart;
	CMyExtEdit m_standortsindex;
	CMyComboBox m_skyddszoner;
	CMyComboBox m_markpaverkan;
	CMyComboBox m_ytstruktur;
	CMyComboBox m_markfukt;
	CMyComboBox m_svarighetsgrad;
	CMyComboBox m_naturvardes;
	CMyComboBox m_nedskrapning;
	CMyComboBox m_lutning;
	CMyComboBox m_uppfrysningsmark;
	CMyComboBox m_fornminnen;
	CMyComboBox m_skador;
	CMyExtEdit m_anteckningar;
	CMyComboBox m_vegtyp;
	CMyComboBox m_kulturlamning;
	CMyExtEdit m_koordx;
	CMyExtEdit m_koordy;
	CMyComboBox m_myr;
	CMyComboBox m_hallmark;
	CMyComboBox m_omrskyddszon;
	CMyComboBox m_objskots;
};
