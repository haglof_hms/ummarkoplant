#pragma once

class CDBFunc;


// CTraktSelectList dialog

class CTraktSelectList : public CFormView
{
	DECLARE_DYNCREATE(CTraktSelectList)

protected:
	CTraktSelectList();
	virtual ~CTraktSelectList();

public:
	void PopulateReport();
	bool SetupReport();

	virtual void OnInitialUpdate();

// Dialog Data
	enum { IDD = IDD_TRAKTSELECT };

protected:
	CDBFunc *m_pDB;
	CMyReportCtrl m_report;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnReportItemClick(NMHDR *pNotifyStruct, LRESULT *pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
};




///////////////////////////////////////////////////////////////////////////////////////////
// CTraktSelectListFrame frame

class CTraktSelectListFrame : public CXTPFrameWndBase<CMDIChildWnd>
{
	DECLARE_DYNCREATE(CTraktSelectListFrame)

public:
	CTraktSelectListFrame();
	virtual ~CTraktSelectListFrame();

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	BOOL m_bFirstOpen;

	afx_msg void OnClose();
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	DECLARE_MESSAGE_MAP()
};
