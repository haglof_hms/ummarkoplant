#pragma once

class CTraktFrame : public CXTPFrameWndBase<CMDIChildWnd>
{
	DECLARE_DYNCREATE(CTraktFrame)

public:
	CTraktFrame();
	virtual ~CTraktFrame();

protected:
	// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	// Message handlers
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

	LRESULT OnMessageFromShell(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()

	bool m_bOnce;
	CString m_sLangFN;
};
