#pragma once

class CDBFunc;


// CExportData form view

class CExportDataView : public CFormView
{
	DECLARE_DYNCREATE(CExportDataView)

protected:
	CExportDataView();
	virtual ~CExportDataView();

public:
	virtual void OnInitialUpdate();

	void ShowExportDialog();

// Dialog Data
	enum { IDD = IDD_EXPORT };

protected:
	CDBFunc *m_pDB;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);

	DECLARE_MESSAGE_MAP()
};




///////////////////////////////////////////////////////////////////////////////////////////
// CExportDataFrame

class CExportDataFrame : public CXTPFrameWndBase<CMDIChildWnd>
{
	DECLARE_DYNCREATE(CExportDataFrame)

public:
	CExportDataFrame();
	virtual ~CExportDataFrame();

	virtual void ActivateFrame(int nCmdShow);

	/*virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	BOOL m_bFirstOpen;

	afx_msg void OnClose();
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);*/

	DECLARE_MESSAGE_MAP()
};
