#include "stdafx.h"
#include "TeamFrame.h"
#include "TeamView.h"
#include "UMMarkoPlantGenerics.h"

/////////////////////////////////////////////////////////////////////////////
// CTeamFrame

IMPLEMENT_DYNCREATE(CTeamFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CTeamFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CTeamFrame)
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_SHOWWINDOW()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CTeamFrame::CTeamFrame()
{
}

CTeamFrame::~CTeamFrame()
{
}

int CTeamFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_bFirstOpen = TRUE;

	// Set up language filename
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);

	return 0;
}

void CTeamFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	// Save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_MARKOPLANT_PLOTVIEWSELECT);
	SavePlacement(this, csBuf);

	CMDIChildWnd::OnClose();
}

BOOL CTeamFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CTeamFrame::OnSetFocus(CWnd *pOldWnd)
{
	// Let form view set up toolbar buttons
	CTeamView *pView = (CTeamView*)getFormViewByID( IDD_FORMTEAM );
	if( pView )
	{
		pView->SetupToolbarButtons();
	}
}

void CTeamFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	// Load window placement
	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	{
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_MARKOPLANT_TEAMVIEW);
		LoadPlacement(this, csBuf);
	}
}

LRESULT CTeamFrame::OnMessageFromShell(WPARAM wParam, LPARAM lParam)
{
	switch( wParam )
	{
	case ID_DBNAVIG_LIST:
		showFormView( IDD_TEAMSELECT, m_sLangFN );
		break;

	default:
		// Forward message to view(s)
		CDocument *pDoc = GetActiveDocument();
		if( pDoc )
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while( pos )
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage( MSG_IN_SUITE, wParam, lParam );
			}
		}
		break;
	}

	return 0L;
}
