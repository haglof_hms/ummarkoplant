#pragma once

#include "DBFunc.h"


// CPlotView form view

class CPlotView : public CFormView
{
	DECLARE_DYNCREATE(CPlotView)

protected:
	CPlotView();
	virtual ~CPlotView();

	void DeletePlot();

	CDBFunc *m_pDB;
	int m_nDBIndex;
	int m_nPlantIndex;
	PlotVector m_plotData;

public:
	void PopulateData(UINT idx = 0);
	void PopulatePlantData();
	void ReloadPlotData(bool bUpdateToolbar = true);
	void SetupToolbarButtons();

	virtual void OnInitialUpdate();

	enum { IDD = IDD_FORMPLOT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg int  OnCreate(LPCREATESTRUCT lpcs);
	afx_msg BOOL OnCopyData(CWnd *pWnd, COPYDATASTRUCT *pCopyDataStruct);

	afx_msg void OnBnClickedNextPlant();
	afx_msg void OnBnClickedPrevPlant();
	DECLARE_MESSAGE_MAP()

	LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);

	CMyExtEdit m_region;
	CMyExtEdit m_distrikt;
	CMyExtEdit m_lag;
	CMyExtEdit m_karta;
	CMyExtEdit m_ytnr;
	CMyExtEdit m_mbpunkt1;
	CMyExtEdit m_mbpunkt2;
	CMyExtEdit m_mbpunkt3;
	CMyExtEdit m_mbpunkt4;
	CMyExtEdit m_mbpunkt5;
	CMyExtEdit m_mbpunkt6;
	CMyExtEdit m_mbpunkt7;
	CMyExtEdit m_mbpunkt8;
	CMyExtEdit m_mbpunkt9;
	CMyExtEdit m_antalspar;
	CMyExtEdit m_sparbredd;
	CMyExtEdit m_antalflackar;
	CMyExtEdit m_arealperflack;
	CMyExtEdit m_sfpbarr;
	CMyExtEdit m_sfplov;
	CMyComboBox m_ratttradslag;
	CMyExtEdit m_koordx;
	CMyExtEdit m_koordy;
	CMyComboBox m_grundforhallande;
	CMyComboBox m_ytstruktur;
	CMyComboBox m_lutning;
	CMyComboBox m_vegtyp;
	CMyComboBox m_jordart;
	CMyComboBox m_markfukt;
	CMyComboBox m_uppfrysningsmark;
	CMyComboBox m_rattmbmetod;
	CMyExtEdit m_standortsindex;
	CMyComboBox m_markberedning;
	CMyComboBox m_orsak;
	CMyComboBox m_svarighetsgrad;
	CMyExtEdit m_plPlantnr;
	CMyComboBox m_plTradslag;
	CMyComboBox m_plVitalitet;
	CMyComboBox m_plOrsak;
	CMyComboBox m_plPlanteringspunkt;
	CMyComboBox m_plFinnsbattre;
	CMyComboBox m_plPlanteringsdjup;
	CMyComboBox m_plTilltryckning;
};
