#include "stdafx.h"
#include "DBFunc.h"
#include "TeamSelectList.h"
#include "TeamView.h"
#include "UMMarkoPlantGenerics.h"

/////////////////////////////////////////////////////////////////////////////
// CTeamView

IMPLEMENT_DYNCREATE(CTeamView, CFormView)

BEGIN_MESSAGE_MAP(CTeamView, CFormView)
	//{{AFX_MSG_MAP(CTeamView)
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_CBN_SELCHANGE(IDC_COMBO1, &OnCbnSelchangeRegion)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


CTeamView::CTeamView():
CFormView(CTeamView::IDD),
m_bAdding(false),
m_pDB(NULL),
m_nDBIndex(0)
{
}

CTeamView::~CTeamView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CTeamView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_region);
	DDX_Control(pDX, IDC_COMBO2, m_distrikt);
	DDX_Control(pDX, IDC_EDIT1, m_lagnr);
	DDX_Control(pDX, IDC_EDIT2, m_lagnamn);
}

void CTeamView::BeginAdding()
{
	m_region.SetReadOnly(FALSE);
	m_distrikt.SetReadOnly(FALSE);
	m_lagnr.SetReadOnly(FALSE);
	m_bAdding = true;
}

void CTeamView::EndAdding()
{
	m_region.SetReadOnly(TRUE);
	m_distrikt.SetReadOnly(TRUE);
	m_lagnr.SetReadOnly(TRUE);
	m_bAdding = false;
}

void CTeamView::DeleteTeam()
{
	CString msg;
	TEAMHEADER &th = m_teamData[m_nDBIndex];

	// If we're adding a record that hasn't been saved yet -- just reload trakt data to get rid of it
	if( m_bAdding )
	{
		ReloadTeamData();
		return;
	}

	// Make sure record exist
	if( m_nDBIndex >= (int)m_teamData.size() )
		return;

	// Bring up confirm dialog
	msg.Format( _T("<center><font size=\"+4\"><b>Ta bort lag</b></font></center><br><hr><br>")
				_T("Region: <b>%d</b><br>Distrikt: <b>%d</b><br>Lagnr: <b>%d</b><br>Lagnamn: <b>%s</b><br><hr><br>")
				_T("<center><font color=\"red\" size=\"+4\">Vill du ta bort detta lag?</font></center>"),
				th.region, th.distrikt, th.lagnr, th.lagnamn );
	if( messageDialog(_T("Ta bort lag"), _T("Ta bort"), _T("Avbryt"), msg) )
	{
		// OK - go on remove trakt
		if( m_pDB->DeleteTeam(th) )
		{
			ReloadTeamData();
		}
	}

	EndAdding();
}

void CTeamView::SaveTeam()
{
	TEAMHEADER *th;

	// Make sure all required fields are filled in if we're adding a record
	if( m_bAdding )
	{
		if( m_region.getText().IsEmpty() || m_distrikt.getText().IsEmpty() || m_lagnr.getText().IsEmpty() )
		{
			AfxMessageBox(_T("Du m�ste ange samtliga nyckelv�rden innan trakten kan sparas!\n\nSe till att region, distrikt och lagnr �r angivet."));
			return;
		}
	}

	// Save current team
	if( m_nDBIndex >= 0 && m_nDBIndex < (int)m_teamData.size() )
	{
		th = &m_teamData[m_nDBIndex];

		// Always save these vars
		th->lagnamn = m_lagnamn.getText();

		// Add or update record
		if( m_bAdding )
		{
			// Set up key vars
			th->region		= int(m_region.GetItemData(m_region.GetCurSel()));
			th->distrikt	= int(m_distrikt.GetItemData(m_distrikt.GetCurSel()));
			th->lagnr		= m_lagnr.getInt();

			if( !m_pDB->AddTeam(*th) )
			{
				ReloadTeamData();
			}
		}
		else
		{
			if( !m_pDB->UpdateTeam(*th) )
			{
				ReloadTeamData();
			}
		}
	}

	EndAdding();
}

void CTeamView::PopulateData(int idx /*=-1*/)
{
	CString str, tmp;
	TEAMHEADER *th;

	// Update index if specified (if we aren't adding a record)
	if( idx >= 0 && !m_bAdding )
		m_nDBIndex = idx;

	if( m_nDBIndex >= 0 && m_nDBIndex < (int)m_teamData.size() && !m_bAdding )
	{
		th = &m_teamData[m_nDBIndex];

		// Load cached data
		m_pDB->GetRegionName(th->region, tmp);
		str.Format(_T("%d - %s"), th->region, tmp);
		m_region.SetWindowText(str);

		ListDistricts(th->region); // Update list of districts

		m_pDB->GetDistrictName(th->distrikt, th->region, tmp);
		str.Format(_T("%d - %s"), th->distrikt, tmp);
		m_distrikt.SetWindowText(str);

		m_lagnr.setInt(th->lagnr);
		m_lagnamn.SetWindowText(th->lagnamn);
	}
	else
	{
		m_region.SetCurSel(-1);
		m_distrikt.SetCurSel(-1);
		m_lagnr.SetWindowText(_T(""));
		m_lagnamn.SetWindowText(_T(""));
	}
}

void CTeamView::SetupToolbarButtons()
{
	BOOL startPrev, endNext;

	// Enable/disable db navig buttons depending on where in the list we are
	if( m_teamData.size() <= 1 )
	{
		// Only one record or less
		startPrev = FALSE;
		endNext = FALSE;
	}
	else if( m_nDBIndex <= 0 )
	{
		// At the beginning
		startPrev = FALSE;
		endNext = TRUE;
	}
	else if( m_nDBIndex >= ((int)m_teamData.size() - 1) )
	{
		// At the end
		startPrev = TRUE;
		endNext = FALSE;
	}
	else
	{
		// In the middle somewhere
		startPrev = TRUE;
		endNext = TRUE;
	}

	// DB naviagation
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,startPrev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,startPrev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,endNext);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,endNext);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	// Tools
	BOOL itemsLeft = m_teamData.size() ? TRUE : FALSE; // enable only if we have any records
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,itemsLeft);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,itemsLeft);
}

// Reloads team data.
// Note: this function should only be called when changes have been made to the database.
void CTeamView::ReloadTeamData( bool bDataAdded /*=false*/ )
{
	if( !m_pDB ) return;

	// End adding so populate is done correctly in case we get here because of an error
	EndAdding();

	// Update team data
	m_pDB->GetTeamData( m_teamData );

	// Check if data was added
	if( bDataAdded )
	{
		// Jump to last record
		m_nDBIndex = (UINT)(m_teamData.size() - 1);
	}
	else
	{
		// Data removed - revert to previous record
		if( --m_nDBIndex < 0 )
			m_nDBIndex = 0;
	}

	PopulateData( m_nDBIndex );
	SetupToolbarButtons();

	// Update team list in case window is open
	CTeamSelectList *pTSL = (CTeamSelectList*)getFormViewByID( IDD_TEAMSELECT );
	if( pTSL )
	{
		pTSL->PopulateReport();
	}
}

void CTeamView::OnInitialUpdate()
{
	CString str, tmp;
	IdList ids;
	IdList::const_iterator iter;
	int idx;

	CFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	// Fill region combo with possible values
	m_pDB->GetRegionList(ids);
	iter = ids.begin();
	while( iter != ids.end() )
	{
		m_pDB->GetRegionName(*iter, tmp);
		str.Format(_T("%d - %s"), *iter, tmp);
		idx = m_region.AddString(str);
		m_region.SetItemData(idx, *iter); // Store id for each string value
		iter++;
	}

	// Disable fields
	m_region.SetDisabledColor(BLACK, INFOBK);
	m_distrikt.SetDisabledColor(BLACK, INFOBK);
	m_lagnr.SetDisabledColor(BLACK, INFOBK);

	m_region.SetReadOnly();
	m_distrikt.SetReadOnly();
	m_lagnr.SetReadOnly();


	// Cache trakt data from db
	if( m_pDB )
	{
		m_pDB->GetTeamData( m_teamData );
	}

	PopulateData( m_nDBIndex );
}

void CTeamView::ListDistricts(int regionId)
{
	CString str, tmp;
	IdList ids;
	IdList::const_iterator iter;
	int idx;

	// Fill district combo with possible values (under current region)
	m_distrikt.ResetContent();
	m_pDB->GetDistrictList(regionId, ids);
	iter = ids.begin();
	while( iter != ids.end() )
	{
		m_pDB->GetDistrictName(*iter, regionId, tmp);
		str.Format(_T("%d - %s"), *iter, tmp);
		idx = m_distrikt.AddString(str);
		m_distrikt.SetItemData(idx, *iter);
		iter++;
	}
}


// CTeamView message handlers

void CTeamView::OnCbnSelchangeRegion()
{
	// Update district combo
	ListDistricts(int(m_region.GetItemData(m_region.GetCurSel())));
}

BOOL CTeamView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}
	}

	return CFormView::OnCopyData(pWnd, pData);
}

int CTeamView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CFormView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	return 0;
}

LRESULT CTeamView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{
	TEAMHEADER th;

	if( m_bAdding && (wParam == ID_DBNAVIG_START || wParam == ID_DBNAVIG_END || wParam == ID_DBNAVIG_PREV || wParam == ID_DBNAVIG_NEXT) )
	{
		AfxMessageBox(_T("Du m�ste antingen spara eller ta bort det aktuella laget innan du kan forts�tta."), MB_OK | MB_ICONINFORMATION);
		return 0;
	}

	switch(wParam)
	{
	case ID_NEW_ITEM:
		m_teamData.push_back(th);
		m_nDBIndex = (int)m_teamData.size() - 1;
		BeginAdding();

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_SAVE_ITEM:
		SaveTeam();
		break;

	case ID_DELETE_ITEM:
		DeleteTeam();
		break;

	case ID_DBNAVIG_START:
		m_nDBIndex = 0;

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_DBNAVIG_PREV:
		if( --m_nDBIndex < 0 )
			m_nDBIndex = 0;

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_DBNAVIG_NEXT:
		if( ++m_nDBIndex > ((int)m_teamData.size() - 1) )
			m_nDBIndex = (int)m_teamData.size() - 1;

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_DBNAVIG_END:
		m_nDBIndex = ((int)m_teamData.size() - 1);

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;
	};

	return 0;
}
