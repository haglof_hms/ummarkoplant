#include "stdafx.h"
#include "DBFunc.h"
#include "UMMarkoPlantGenerics.h"
#include "UMMarkoPlantStrings.h"
#include "PlotSelectList.h"
#include "PlotView.h"


// CPlotView

IMPLEMENT_DYNCREATE(CPlotView, CFormView)

BEGIN_MESSAGE_MAP(CPlotView, CFormView)
	//{{AFX_MSG_MAP(CPlotView)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)

	ON_BN_CLICKED(IDC_BUTTON1, &OnBnClickedPrevPlant)
	ON_BN_CLICKED(IDC_BUTTON2, &OnBnClickedNextPlant)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPlotView::CPlotView():
CFormView(CPlotView::IDD),
m_pDB(NULL),
m_nDBIndex(0),
m_nPlantIndex(0)
{
}

CPlotView::~CPlotView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CPlotView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT7, m_region);
	DDX_Control(pDX, IDC_EDIT10, m_distrikt);
	DDX_Control(pDX, IDC_EDIT6, m_lag);
	DDX_Control(pDX, IDC_EDIT8, m_karta);
	DDX_Control(pDX, IDC_EDIT11, m_ytnr);
	DDX_Control(pDX, IDC_EDIT9, m_mbpunkt1);
	DDX_Control(pDX, IDC_EDIT14, m_mbpunkt2);
	DDX_Control(pDX, IDC_EDIT12, m_mbpunkt3);
	DDX_Control(pDX, IDC_EDIT15, m_mbpunkt4);
	DDX_Control(pDX, IDC_EDIT20, m_mbpunkt5);
	DDX_Control(pDX, IDC_EDIT16, m_mbpunkt6);
	DDX_Control(pDX, IDC_EDIT17, m_mbpunkt7);
	DDX_Control(pDX, IDC_EDIT18, m_mbpunkt8);
	DDX_Control(pDX, IDC_EDIT19, m_mbpunkt9);
	DDX_Control(pDX, IDC_EDIT28, m_antalspar);
	DDX_Control(pDX, IDC_EDIT29, m_sparbredd);
	DDX_Control(pDX, IDC_EDIT30, m_antalflackar);
	DDX_Control(pDX, IDC_EDIT31, m_arealperflack);
	DDX_Control(pDX, IDC_EDIT26, m_sfpbarr);
	DDX_Control(pDX, IDC_EDIT27, m_sfplov);
	DDX_Control(pDX, IDC_COMBO22, m_ratttradslag);
	DDX_Control(pDX, IDC_EDIT24, m_koordx);
	DDX_Control(pDX, IDC_EDIT25, m_koordy);
	DDX_Control(pDX, IDC_COMBO6, m_grundforhallande);
	DDX_Control(pDX, IDC_COMBO7, m_ytstruktur);
	DDX_Control(pDX, IDC_COMBO8, m_lutning);
	DDX_Control(pDX, IDC_COMBO9, m_vegtyp);
	DDX_Control(pDX, IDC_COMBO10, m_jordart);
	DDX_Control(pDX, IDC_COMBO11, m_markfukt);
	DDX_Control(pDX, IDC_COMBO12, m_uppfrysningsmark);
	DDX_Control(pDX, IDC_COMBO29, m_rattmbmetod);
	DDX_Control(pDX, IDC_EDIT22, m_standortsindex);
	DDX_Control(pDX, IDC_COMBO13, m_markberedning);
	DDX_Control(pDX, IDC_COMBO14, m_orsak);
	DDX_Control(pDX, IDC_COMBO16, m_svarighetsgrad);
	DDX_Control(pDX, IDC_EDIT32, m_plPlantnr);
	DDX_Control(pDX, IDC_COMBO15, m_plTradslag);
	DDX_Control(pDX, IDC_COMBO20, m_plVitalitet);
	DDX_Control(pDX, IDC_COMBO21, m_plOrsak);
	DDX_Control(pDX, IDC_COMBO23, m_plPlanteringspunkt);
	DDX_Control(pDX, IDC_COMBO24, m_plFinnsbattre);
	DDX_Control(pDX, IDC_COMBO27, m_plPlanteringsdjup);
	DDX_Control(pDX, IDC_COMBO28, m_plTilltryckning);
}

void CPlotView::PopulateData(UINT idx /*=0*/)
{
	int i;
	CString str, tmp;
	PLOTHEADER *ph;

	// Update index if specified, just to first plant
	if( idx )
	{
		m_nDBIndex = idx;
		m_nPlantIndex = 0;
	}

	if( m_nDBIndex >= 0 && m_nDBIndex < (int)m_plotData.size() )
	{
		ph = &m_plotData[m_nDBIndex];

		// Load cached data
		m_pDB->GetRegionName(_tstoi(ph->region), tmp);
		str.Format(_T("%s - %s"), ph->region, tmp);
		m_region.SetWindowText(str);

		tmp.Empty();
		m_pDB->GetDistrictName(_tstoi(ph->distrikt), _tstoi(ph->region), tmp);
		str.Format(_T("%s - %s"), ph->distrikt, tmp);
		m_distrikt.SetWindowText(str);

		tmp.Empty();
		m_pDB->GetTeamName(_tstoi(ph->lag), _tstoi(ph->distrikt), _tstoi(ph->region), tmp);
		str.Format(_T("%s - %s"), ph->lag, tmp);
		m_lag.SetWindowText(str);

		m_ytnr.setInt(ph->lopnr);
		if( ph->vars[0] == 0 )	m_mbpunkt1.SetWindowText(_T("0"));
		else					m_mbpunkt1.setInt(ph->vars[0]);
		if( ph->vars[1] == 0 )	m_mbpunkt2.SetWindowText(_T("0"));
		else					m_mbpunkt2.setInt(ph->vars[1]);
		if( ph->vars[2] == 0 )	m_mbpunkt3.SetWindowText(_T("0"));
		else					m_mbpunkt3.setInt(ph->vars[2]);
		if( ph->vars[3] == 0 )	m_mbpunkt4.SetWindowText(_T("0"));
		else					m_mbpunkt4.setInt(ph->vars[3]);
		if( ph->vars[4] == 0 )	m_mbpunkt5.SetWindowText(_T("0"));
		else					m_mbpunkt5.setInt(ph->vars[4]);
		if( ph->vars[5] == 0 )	m_mbpunkt6.SetWindowText(_T("0"));
		else					m_mbpunkt6.setInt(ph->vars[5]);
		if( ph->vars[6] == 0 )	m_mbpunkt7.SetWindowText(_T("0"));
		else					m_mbpunkt7.setInt(ph->vars[6]);
		if( ph->vars[7] == 0 )	m_mbpunkt8.SetWindowText(_T("0"));
		else					m_mbpunkt8.setInt(ph->vars[7]);
		if( ph->vars[8] == 0 )	m_mbpunkt9.SetWindowText(_T("0"));
		else					m_mbpunkt9.setInt(ph->vars[8]);

		if( ph->antalSpar > 0 )			m_antalspar.setInt(ph->antalSpar);
		else if( ph->antalSpar == 0 )	m_antalspar.SetWindowText(_T("0"));
		else							m_antalspar.SetWindowText(_T(""));
		if( ph->sparBredd > 0 )			m_sparbredd.setInt(ph->sparBredd);
		else if( ph->sparBredd == 0 )	m_sparbredd.SetWindowText(_T("0"));
		else							m_sparbredd.SetWindowText(_T(""));
		if( ph->antalFlackar > 0 )		m_antalflackar.setInt(ph->antalFlackar);
		else if( ph->antalFlackar == 0 )m_antalflackar.SetWindowText(_T("0"));
		else							m_antalflackar.SetWindowText(_T(""));
		if( ph->arealPerFlack > 0 )		m_arealperflack.setFloat(ph->arealPerFlack, 1);
		else if( ph->arealPerFlack == 0)m_arealperflack.SetWindowText(_T("0"));
		else							m_arealperflack.SetWindowText(_T(""));
		if( ph->sfpBarr > 0 )			m_sfpbarr.setInt(ph->sfpBarr);
		else if( ph->sfpBarr == 0 )		m_sfpbarr.SetWindowText(_T("0"));
		else							m_sfpbarr.SetWindowText(_T(""));
		if( ph->sfpLov > 0 )			m_sfplov.setInt(ph->sfpLov);
		else if( ph->sfpLov == 0 )		m_sfplov.SetWindowText(_T("0"));
		else							m_sfplov.SetWindowText(_T(""));

		m_karta.SetWindowText(ph->karta);
		m_koordx.SetWindowText(ph->koordinatX);
		m_koordy.SetWindowText(ph->koordinatY);
		m_standortsindex.SetWindowText(ph->si);

		if( ph->vegTyp == -1 )
		{
			m_vegtyp.SetCurSel(-1);
		}
		else
		{
			for( i = 0; i < VEGTYP_CT; i++ )
			{
				if( ph->vegTyp == VEGTYP_INDICES[i] )
				{
					m_vegtyp.SetCurSel(i);
				}
			}
		}

		if( ph->jordart == -1 )
		{
			m_jordart.SetCurSel(-1);
		}
		else
		{
			for( i = 0; i < JORDART_CT; i++ )
			{
				if( ph->jordart == JORDART_INDICES[i] )
				{
					m_jordart.SetCurSel(i);
				}
			}
		}

		m_ratttradslag.SetCurSel(MAX(ph->rattTradslag - 1, -1));
		m_grundforhallande.SetCurSel(ph->grundfor);
		m_ytstruktur.SetCurSel(ph->ytstruktur);
		m_lutning.SetCurSel(ph->lutning);
		m_markfukt.SetCurSel(ph->markfukt);
		m_uppfrysningsmark.SetCurSel(MAX(ph->uppfr - 1, -1));
		m_rattmbmetod.SetCurSel(MAX(ph->rattMbMetod - 1, -1));
		m_markberedning.SetCurSel(MAX(ph->markberedning - 1, -1));
		m_orsak.SetCurSel(ph->orsak);
		m_svarighetsgrad.SetCurSel(ph->svarighet);

		// Check if this plot contain any plants
		if( ph->plants.size() > 0 )
		{
			GetDlgItem(IDC_BUTTON1)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON2)->EnableWindow(TRUE);
		}
		else
		{
			GetDlgItem(IDC_BUTTON1)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON2)->EnableWindow(FALSE);
		}
	}
	else
	{
		// Clear fields
		m_region.SetWindowText(_T(""));
		m_distrikt.SetWindowText(_T(""));
		m_lag.SetWindowText(_T(""));
		m_karta.SetWindowText(_T(""));
		m_ytnr.SetWindowText(_T(""));
		m_mbpunkt1.SetWindowText(_T(""));
		m_mbpunkt2.SetWindowText(_T(""));
		m_mbpunkt3.SetWindowText(_T(""));
		m_mbpunkt4.SetWindowText(_T(""));
		m_mbpunkt5.SetWindowText(_T(""));
		m_mbpunkt6.SetWindowText(_T(""));
		m_mbpunkt7.SetWindowText(_T(""));
		m_mbpunkt8.SetWindowText(_T(""));
		m_mbpunkt9.SetWindowText(_T(""));
		m_antalspar.SetWindowText(_T(""));
		m_sparbredd.SetWindowText(_T(""));
		m_antalflackar.SetWindowText(_T(""));
		m_arealperflack.SetWindowText(_T(""));
		m_sfpbarr.SetWindowText(_T(""));
		m_sfplov.SetWindowText(_T(""));
		m_koordx.SetWindowText(_T(""));
		m_koordy.SetWindowText(_T(""));
		m_standortsindex.SetWindowText(_T(""));

		m_ratttradslag.SetCurSel(-1);
		m_grundforhallande.SetCurSel(-1);
		m_ytstruktur.SetCurSel(-1);
		m_lutning.SetCurSel(-1);
		m_vegtyp.SetCurSel(-1);
		m_jordart.SetCurSel(-1);
		m_markfukt.SetCurSel(-1);
		m_uppfrysningsmark.SetCurSel(-1);
		m_rattmbmetod.SetCurSel(-1);
		m_markberedning.SetCurSel(-1);
		m_orsak.SetCurSel(-1);
		m_svarighetsgrad.SetCurSel(-1);
	}

	// Update plant data
	PopulatePlantData();
}

void CPlotView::PopulatePlantData()
{
	bool clearFields = true;
	CString str;
	PLANTHEADER *plant;

	if( m_nDBIndex >= 0 && m_nDBIndex < (int)m_plotData.size() )
	{
		if( m_nPlantIndex >= 0 && m_nPlantIndex < (int)m_plotData[m_nDBIndex].plants.size() )
		{
			plant = &m_plotData[m_nDBIndex].plants[m_nPlantIndex];

			// Display cached plant data
			str.Format(_T("%d"), m_nPlantIndex + 1);
			m_plPlantnr.SetWindowText(str);

			// Match tr�dslag mot index
			if( plant->tradslag == -1 )
			{
				m_plTradslag.SetCurSel(-1);
			}
			else
			{
				for( int i = 0; i < sizeof(TRADSLAG_INDICES) / sizeof(int); i++ )
				{
					if( plant->tradslag == TRADSLAG_INDICES[i] )
					{
						m_plTradslag.SetCurSel(i);
					}
				}
			}

			m_plVitalitet.SetCurSel(MAX(plant->vitalitet - 1, -1));
			m_plOrsak.SetCurSel(MAX(plant->orsak - 1, -1));
			m_plPlanteringspunkt.SetCurSel(plant->planteringspunkt);
			m_plFinnsbattre.SetCurSel(MAX(plant->battre - 1, -1));
			m_plPlanteringsdjup.SetCurSel(MAX(plant->planteringsdjup - 1, -1));
			m_plTilltryckning.SetCurSel(MAX(plant->tilltryckning - 1, -1));

			clearFields = false;
		}
	}

	if( clearFields )
	{
		// Clear fields
		m_plPlantnr.SetWindowText(_T(""));
		m_plTradslag.SetCurSel(-1);
		m_plVitalitet.SetCurSel(-1);
		m_plOrsak.SetCurSel(-1);
		m_plPlanteringspunkt.SetCurSel(-1);
		m_plFinnsbattre.SetCurSel(-1);
		m_plPlanteringsdjup.SetCurSel(-1);
		m_plTilltryckning.SetCurSel(-1);
	}
}

void CPlotView::DeletePlot()
{
	CString msg;
	PLOTHEADER &ph = m_plotData[m_nDBIndex];

	// Make sure record exist
	if( m_nDBIndex >= (int)m_plotData.size() )
		return;

	// Bring up confirm dialog
	msg.Format( _T("<center><font size=\"+4\"><b>Ta bort yta</b></font></center><br><hr><br>")
				_T("Ytnr: <b>%d</b><br><hr><br>")
				_T("<center><font color=\"red\" size=\"+4\">Vill du ta bort denna yta?</font></center>"),
				ph.lopnr );
	if( messageDialog(_T("Ta bort yta"), _T("Ta bort"), _T("Avbryt"), msg) )
	{
		// OK - go on remove plot
		if( m_pDB->DeletePlot(ph) )
		{
			ReloadPlotData();
		}
	}
}

void CPlotView::ReloadPlotData(bool bUpdateToolbar /*=true*/)
{
	if( !m_pDB ) return;

	// Update plot data
	m_pDB->GetPlotData( m_plotData );

	// Just to last record if we're out of bounds
	if( m_nDBIndex >= (int)m_plotData.size() )
		m_nDBIndex = max(0, (int)m_plotData.size() - 1);
	
	PopulateData(m_nDBIndex);

	// Update toolbar only if specified, we don't want to interfere with TraktView
	if( bUpdateToolbar )
		SetupToolbarButtons();


	// Update plot list view
	CPlotSelectList *pPSL = (CPlotSelectList*)getFormViewByID( IDD_PLOTSELECT );
	if( pPSL )
	{
		pPSL->PopulateReport();
	}
}

void CPlotView::SetupToolbarButtons()
{
	BOOL startPrev, endNext;

	// Enable/disable db navig buttons depending on where in the list we are
	if( m_plotData.size() <= 1 )
	{
		// Only one record or less
		startPrev = FALSE;
		endNext = FALSE;
	}
	else if( m_nDBIndex <= 0 )
	{
		// At the beginning
		startPrev = FALSE;
		endNext = TRUE;
	}
	else if( m_nDBIndex >= ((int)m_plotData.size() - 1) )
	{
		// At the end
		startPrev = TRUE;
		endNext = FALSE;
	}
	else
	{
		// In the middle somewhere
		startPrev = TRUE;
		endNext = TRUE;
	}

	// DB navigation
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,startPrev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,startPrev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,endNext);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,endNext);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	// Tools
	BOOL deleteItem = m_plotData.size() ? TRUE : FALSE; // enable only if we have any records
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,deleteItem);
}

void CPlotView::OnInitialUpdate()
{
	int i;

	CFormView::OnInitialUpdate();

	//SetScaleToFitSize(CSize(90, 1));

	// Extract filename of this module
	TCHAR szBuf[MAX_PATH], szModule[MAX_PATH];
	GetModuleFileName(g_hInstance, szBuf, sizeof(szBuf) / sizeof(TCHAR));
	_tsplitpath(szBuf, NULL, NULL, szModule, NULL);

	// Check if we have a valid license
	_user_msg msg(820, _T("CheckLicense"), _T("License.dll"), _T("H9000"), _T(""), _T(""), szModule);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&msg);
#ifdef UNICODE
	if( _tcscmp(msg.getSuite(), _T("0")) == 0 ||
		_tcscmp(msg.getSuite(), _T("License.dll")) == 0 )
	{
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}
#else
	if( _tcscmp(msg.getSuite(), _T("-1")) == 0 ||
		_tcscmp(msg.getSuite(), _T("License.dll")) == 0 )
	{
		AfxMessageBox(_T("Ingen licens!"));
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}
#endif


	// First of all, make sure required tables exist
	m_pDB->MakeSureTablesExist();

	// Fill up combo boxes with possible values
	for( i = 0; i < GRUNDFORHALLANDE_CT; i++ )
	{
		m_grundforhallande.AddString(GRUNDFORHALLANDE[i]);
	}
	for( i = 0; i < YTSTRUKTUR_CT; i++ )
	{
		m_ytstruktur.AddString(YTSTRUKTUR[i]);
	}
	for( i = 0; i < LUTNING_CT; i++ )
	{
		m_lutning.AddString(LUTNING[i]);
	}
	for( i = 0; i < VEGTYP_CT; i++ )
	{
		m_vegtyp.AddString(VEGTYP[i]);
	}
	for( i = 0; i < JORDART_CT; i++ )
	{
		m_jordart.AddString(JORDART[i]);
	}
	for( i = 0; i < MARKFUKT_CT; i++ )
	{
		m_markfukt.AddString(MARKFUKT[i]);
	}
	for( i = 0; i < UPPFRYSNINGSMARK_CT; i++ )
	{
		m_uppfrysningsmark.AddString(UPPFRYSNINGSMARK[i]);
	}
	for( i = 0; i < RATTMBMETOD_CT; i++ )
	{
		m_rattmbmetod.AddString(RATTMBMETOD[i]);
	}
	for( i = 0; i < MARKBEREDNING_CT; i++ )
	{
		m_markberedning.AddString(MARKBEREDNING[i]);
	}
	for( i = 0; i < ORSAK_CT; i++ )
	{
		m_orsak.AddString(ORSAK[i]);
	}
	for( i = 0; i < SVARIGHETSGRAD_CT; i++ )
	{
		m_svarighetsgrad.AddString(SVARIGHETSGRAD[i]);
	}
	for( i = 0; i < RATTTRADSLAG_CT; i++ )
	{
		m_ratttradslag.AddString(RATTTRADSLAG[i]);
	}
	for( i = 0; i < TRADSLAG_CT; i++ )
	{
		m_plTradslag.AddString(TRADSLAG[i]);
	}
	for( i = 0; i < VITALITET_CT; i++ )
	{
		m_plVitalitet.AddString(VITALITET[i]);
	}
	for( i = 0; i < ORSAKPLANTA_CT; i++ )
	{
		m_plOrsak.AddString(ORSAKPLANTA[i]);
	}
	for( i = 0; i < PLANTERINGSPUNKT_CT; i++ )
	{
		m_plPlanteringspunkt.AddString(PLANTERINGSPUNKT[i]);
	}
	for( i = 0; i < FINNSBATTRE_CT; i++ )
	{
		m_plFinnsbattre.AddString(FINNSBATTRE[i]);
	}
	for( i = 0; i < PLANTERINGSDJUP_CT; i++ )
	{
		m_plPlanteringsdjup.AddString(PLANTERINGSDJUP[i]);
	}
	for( i = 0; i < TILLTRYCKNING_CT; i++ )
	{
		m_plTilltryckning.AddString(TILLTRYCKNING[i]);
	}

	// Set fields to read-only
	m_region.SetDisabledColor(BLACK, INFOBK);
	m_distrikt.SetDisabledColor(BLACK, INFOBK);
	m_lag.SetDisabledColor(BLACK, INFOBK);
	m_karta.SetDisabledColor(BLACK, INFOBK);
	m_ytnr.SetDisabledColor(BLACK, INFOBK);
	m_mbpunkt1.SetDisabledColor(BLACK, INFOBK);
	m_mbpunkt2.SetDisabledColor(BLACK, INFOBK);
	m_mbpunkt3.SetDisabledColor(BLACK, INFOBK);
	m_mbpunkt4.SetDisabledColor(BLACK, INFOBK);
	m_mbpunkt5.SetDisabledColor(BLACK, INFOBK);
	m_mbpunkt6.SetDisabledColor(BLACK, INFOBK);
	m_mbpunkt7.SetDisabledColor(BLACK, INFOBK);
	m_mbpunkt8.SetDisabledColor(BLACK, INFOBK);
	m_mbpunkt9.SetDisabledColor(BLACK, INFOBK);
	m_antalspar.SetDisabledColor(BLACK, INFOBK);
	m_sparbredd.SetDisabledColor(BLACK, INFOBK);
	m_antalflackar.SetDisabledColor(BLACK, INFOBK);
	m_arealperflack.SetDisabledColor(BLACK, INFOBK);
	m_sfpbarr.SetDisabledColor(BLACK, INFOBK);
	m_sfplov.SetDisabledColor(BLACK, INFOBK);
	m_ratttradslag.SetDisabledColor(BLACK, INFOBK);
	m_koordx.SetDisabledColor(BLACK, INFOBK);
	m_koordy.SetDisabledColor(BLACK, INFOBK);
	m_grundforhallande.SetDisabledColor(BLACK, INFOBK);
	m_ytstruktur.SetDisabledColor(BLACK, INFOBK);
	m_lutning.SetDisabledColor(BLACK, INFOBK);
	m_vegtyp.SetDisabledColor(BLACK, INFOBK);
	m_jordart.SetDisabledColor(BLACK, INFOBK);
	m_markfukt.SetDisabledColor(BLACK, INFOBK);
	m_uppfrysningsmark.SetDisabledColor(BLACK, INFOBK);
	m_rattmbmetod.SetDisabledColor(BLACK, INFOBK);
	m_standortsindex.SetDisabledColor(BLACK, INFOBK);
	m_markberedning.SetDisabledColor(BLACK, INFOBK);
	m_orsak.SetDisabledColor(BLACK, INFOBK);
	m_svarighetsgrad.SetDisabledColor(BLACK, INFOBK);
	m_plPlantnr.SetDisabledColor(BLACK, INFOBK);
	m_plTradslag.SetDisabledColor(BLACK, INFOBK);
	m_plVitalitet.SetDisabledColor(BLACK, INFOBK);
	m_plOrsak.SetDisabledColor(BLACK, INFOBK);
	m_plPlanteringspunkt.SetDisabledColor(BLACK, INFOBK);
	m_plFinnsbattre.SetDisabledColor(BLACK, INFOBK);
	m_plPlanteringsdjup.SetDisabledColor(BLACK, INFOBK);
	m_plTilltryckning.SetDisabledColor(BLACK, INFOBK);

	m_region.SetReadOnly();
	m_distrikt.SetReadOnly();
	m_lag.SetReadOnly();
	m_karta.SetReadOnly();
	m_ytnr.SetReadOnly();
	m_mbpunkt1.SetReadOnly();
	m_mbpunkt2.SetReadOnly();
	m_mbpunkt3.SetReadOnly();
	m_mbpunkt4.SetReadOnly();
	m_mbpunkt5.SetReadOnly();
	m_mbpunkt6.SetReadOnly();
	m_mbpunkt7.SetReadOnly();
	m_mbpunkt8.SetReadOnly();
	m_mbpunkt9.SetReadOnly();
	m_antalspar.SetReadOnly();
	m_sparbredd.SetReadOnly();
	m_antalflackar.SetReadOnly();
	m_arealperflack.SetReadOnly();
	m_sfpbarr.SetReadOnly();
	m_sfplov.SetReadOnly();
	m_koordx.SetReadOnly();
	m_koordy.SetReadOnly();
	m_standortsindex.SetReadOnly();
	m_plPlantnr.SetReadOnly();

	// Cache plot data from db
	if( m_pDB )
	{
		m_pDB->GetPlotData( m_plotData );
	}

	PopulateData(m_nDBIndex);
}

BOOL CPlotView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}
	}

	return CFormView::OnCopyData(pWnd, pData);
}

int CPlotView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CFormView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	return 0;
}

LRESULT CPlotView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
	case ID_OPEN_ITEM:
		m_pDB->ShowImportDialog();
		break;

	case ID_DELETE_ITEM:
		DeletePlot();
		break;

	case ID_DBNAVIG_START:
		m_nDBIndex = 0;

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_DBNAVIG_PREV:
		if( --m_nDBIndex < 0 )
			m_nDBIndex = 0;

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_DBNAVIG_NEXT:
		if( ++m_nDBIndex > ((int)m_plotData.size() - 1) )
			m_nDBIndex = (int)m_plotData.size() - 1;

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;

	case ID_DBNAVIG_END:
		m_nDBIndex = ((int)m_plotData.size() - 1);

		SetupToolbarButtons();
		PopulateData(m_nDBIndex);
		break;
	};

	return 0;
}

void CPlotView::OnBnClickedNextPlant()
{
	if( ++m_nPlantIndex > ((int)m_plotData[m_nDBIndex].plants.size() - 1) )
		m_nPlantIndex = (int)m_plotData[m_nDBIndex].plants.size() - 1;

	PopulatePlantData();
}

void CPlotView::OnBnClickedPrevPlant()
{
	if( --m_nPlantIndex < 0 )
		m_nPlantIndex = 0;

	PopulatePlantData();
}
