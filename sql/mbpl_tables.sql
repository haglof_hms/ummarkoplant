CREATE TABLE [team_table]
(
	[team_num] int NOT NULL,
	[team_region_num] int NOT NULL,
	[team_district_num] int NOT NULL,
	[team_name] varchar(20) NOT NULL,
	CONSTRAINT [PK_team_table] PRIMARY KEY 
	(
		[team_num] ASC,
		[team_region_num] ASC,
		[team_district_num] ASC
	)
)

CREATE TABLE [mbpl_trakt]
(
	[region] int NOT NULL,
	[distrikt] int NOT NULL,
	[lag] int NOT NULL,
	[karta] int NOT NULL,
	[uppfoljning] smallint NOT NULL,
	[invtyp] smallint NOT NULL,
	[ursprung] smallint NOT NULL,
	[anvandare] varchar(50) NULL,
	[datum] datetime NOT NULL,
	[bestand] smallint NOT NULL,
	[delbestand] smallint NOT NULL,
	[namn] varchar(50) NULL,
	[areal] float NULL,
	[hoh] smallint NOT NULL,
	[radie] float NOT NULL,
	[grundforhallande] smallint NULL,
	[ytstruktur] smallint NULL,
	[lutning] smallint NULL,
	[vegtyp] smallint NULL,
	[jordart] smallint NULL,
	[markfukt] smallint NULL,
	[uppfrysningsmark] smallint NULL,
	[SI] varchar(5) NULL,
	[svarighet] smallint NULL,
	[myr] smallint NULL,
	[hallmark] smallint NULL,
	[omrskyddszon] smallint NULL,
	[objskots] smallint NULL,
	[fornminnen] smallint NULL,
	[kultur] smallint NULL,
	[skyddszoner] smallint NULL,
	[naturvarde] smallint NULL,
	[skador] smallint NULL,
	[koordinatx] varchar(10) NULL,
	[koordinaty] varchar(10) NULL,
	[markpaverkan] smallint NULL,
	[nedskrapning] smallint NULL,
	[text] varchar(50) NULL,
	[avgKravMbStallen] int NULL,
	[avgKontinuerlig] float NULL,
	[avgIntermittent] float NULL,
	[avgPaverkadArealKont] float NULL,
	[avgPaverkadArealInter] float NULL,
	[avgOptimalaMbStallen] float NULL,
	[avgBraMbStallen] float NULL,
	[avgMarkbereddAreal] float NULL,
	[avgOptimalBraMb] float NULL,
	[avgRattMbMetod] float NULL,
	[avgTall] float NULL,
	[avgGran] float NULL,
	[avgSfpBarr] float NULL,
	[avgSfpLov] float NULL,
	[avgOptimal] float NULL,
	[avgBra] float NULL,
	[avgOptimalBraPl] float NULL,
	[avgUtnyttjandegrad] float NULL,
	[avgUPI] float NULL,
	[avgRattTradslag] float NULL,
	[avgRattPlantantal] float NULL,
	CONSTRAINT [PK_trakt] PRIMARY KEY 
	(
		[region],
		[distrikt],
		[lag],
		[karta],
		[bestand],
		[delbestand]
	)
)

CREATE TABLE [mbpl_yta]
(
	[ytid] int IDENTITY NOT NULL,
	[lopnr] int NOT NULL,
	[region] int NOT NULL,
	[distrikt] int NOT NULL,
	[lag] int NOT NULL,
	[karta] int NOT NULL,
	[bestand] smallint NOT NULL,
	[delbestand] smallint NOT NULL,
	[koordinatx] varchar(10) NULL,
	[koordinaty] varchar(10) NULL,
	[grundforhallande] smallint NULL,
	[ytstruktur] smallint NULL,
	[lutning] smallint NULL,
	[vegtyp] smallint NULL,
	[jordart] smallint NOT NULL,
	[markfukt] smallint NOT NULL,
	[uppfrysningsmark] smallint NOT NULL,
	[SI] varchar(50) NOT NULL,
	[tradslag] smallint NOT NULL,
	[markberedning] smallint NOT NULL,
	[orsak] smallint NULL,
	[svarighet] smallint NULL,
	[antalspar] smallint NULL,
	[sparbredd] smallint NULL,
	[antalflackar] smallint NULL,
	[arealperflack] float NULL,
	[mbpunkt1] smallint NOT NULL,
	[mbpunkt2] smallint NOT NULL,
	[mbpunkt3] smallint NOT NULL,
	[mbpunkt4] smallint NOT NULL,
	[mbpunkt5] smallint NOT NULL,
	[mbpunkt6] smallint NOT NULL,
	[mbpunkt7] smallint NOT NULL,
	[mbpunkt8] smallint NOT NULL,
	[mbpunkt9] smallint NOT NULL,
	[sfpbarr] smallint NOT NULL,
	[sfplov] smallint NOT NULL,
	[rattTradslag] smallint NOT NULL,
	[rattMbMetod] smallint NOT NULL,
	CONSTRAINT [PK_yta] PRIMARY KEY 
	(
		[ytid]
	)
)

CREATE TABLE [mbpl_planta]
(
	[ytid] int NOT NULL,
	[tradslag] smallint NOT NULL,
	[vitalitet] smallint NULL,
	[orsak] smallint NULL,
	[planteringspunkt] smallint NOT NULL,
	[finnsbattre] smallint NOT NULL,
	[planteringsdjup] smallint NOT NULL,
	[tilltryckning] smallint NOT NULL
)

ALTER TABLE [mbpl_yta] ADD CONSTRAINT [FK_mbpl_yta_mbpl_trakt] FOREIGN KEY([region], [distrikt], [lag], [karta], [bestand], [delbestand])
REFERENCES [mbpl_trakt] ([region], [distrikt], [lag], [karta], [bestand], [delbestand])
ON UPDATE CASCADE 
ON DELETE CASCADE

ALTER TABLE[mbpl_planta] ADD CONSTRAINT [FK_mbpl_planta_mbpl_yta] FOREIGN KEY([ytid])
REFERENCES [mbpl_yta] ([ytid])
ON UPDATE CASCADE 
ON DELETE CASCADE