#include "stdafx.h"
#include "DBFunc.h"
#include "UMMarkoPlantGenerics.h"
#include "UMMarkoPlant.h"
#include "TraktSelectList.h"
#include "TraktView.h"


/////////////////////////////////////////////////////////////////////////////
// CTraktSelectList

IMPLEMENT_DYNCREATE(CTraktSelectList, CFormView)

BEGIN_MESSAGE_MAP(CTraktSelectList, CFormView)
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY(NM_CLICK, IDC_TRAKTREPORT, OnReportItemClick)
END_MESSAGE_MAP()


CTraktSelectList::CTraktSelectList():
CFormView(CTraktSelectList::IDD),
m_pDB(NULL)
{
}

CTraktSelectList::~CTraktSelectList()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CTraktSelectList::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

void CTraktSelectList::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	SetupReport();
}

void CTraktSelectList::PopulateReport()
{
	CXTPReportRecord *pRec;
	TractVector tracts;

	// Clear report
	m_report.ClearReport();

	// Get data from db
	if( m_pDB )
	{
		m_pDB->GetTraktData( tracts );
	}

	// Add new data
	for( UINT i = 0; i < tracts.size(); i++ )
	{
		// Add new record, set up subitems
		pRec = new CXTPReportRecord;

		//m_pDB->GetRegionName(atoi(tracts[i].region), str);
		pRec->AddItem( new CXTPReportRecordItemText(tracts[i].region) );

		//m_pDB->GetDistrictName(atoi(tracts[i].distrikt), atoi(tracts[i].region), str);
		pRec->AddItem( new CXTPReportRecordItemText(tracts[i].distrikt) );

		pRec->AddItem( new CXTPReportRecordItemText(tracts[i].lag) );
		pRec->AddItem( new CXTPReportRecordItemText(tracts[i].karta) );
		pRec->AddItem( new CXTPReportRecordItemText(tracts[i].bestand) );
		pRec->AddItem( new CXTPReportRecordItemNumber(tracts[i].delbestand) );
		pRec->AddItem( new CXTPReportRecordItemText(tracts[i].namn) );
		pRec->AddItem( new CXTPReportRecordItemText(tracts[i].datum) );
		m_report.AddRecord( pRec );
	}

	m_report.Populate();
	m_report.UpdateWindow();
}

bool CTraktSelectList::SetupReport()
{
	CXTPReportColumn *pCol;

	// Create list view if not already done
	if( m_report.GetSafeHwnd() == NULL )
	{
		if( !m_report.Create(this, IDC_TRAKTREPORT) )
		{
			return false;
		}
	}

	// Set up columns
	m_report.ShowWindow( SW_NORMAL );

	pCol = m_report.AddColumn( new CXTPReportColumn(0, _T("Region"), 50) );
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );

	pCol = m_report.AddColumn( new CXTPReportColumn(1, _T("Distrikt"), 50) );
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );

	pCol = m_report.AddColumn( new CXTPReportColumn(2, _T("Lag"), 50) );
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );

	pCol = m_report.AddColumn( new CXTPReportColumn(3, _T("Karta/Traktnummer"), 50) );
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );

	pCol = m_report.AddColumn( new CXTPReportColumn(4, _T("Best�nd"), 50) );
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );

	pCol = m_report.AddColumn( new CXTPReportColumn(5, _T("Delbest�nd"), 50) );
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );

	pCol = m_report.AddColumn( new CXTPReportColumn(6, _T("Traktnr/best�ndsnamn"), 50) );
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );

	pCol = m_report.AddColumn( new CXTPReportColumn(7, _T("Datum"), 50) );
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );

	OnSize(0,0,0);

	PopulateReport();

	return true;
}


// CTraktSelectList message handlers

BOOL CTraktSelectList::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}
	}

	return CFormView::OnCopyData(pWnd, pData);
}

int CTraktSelectList::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CFormView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	return 0;
}

void CTraktSelectList::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	// Adjust report control size
	if( m_report.GetSafeHwnd() )
	{
		setResize( &m_report, 0, 0, rect.right, rect.bottom );
	}
}

void CTraktSelectList::OnReportItemClick(NMHDR *pNotifyStruct, LRESULT *pResult)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if( pItemNotify->pRow )
	{
		// Update trakt view
		CXTPReportRecord *pRec = (CXTPReportRecord*)pItemNotify->pItem->GetRecord();
		CTraktView *pView = (CTraktView*)getFormViewByID(IDD_FORMTRAKT);
		if( pView )
		{
			pView->PopulateData( pRec->GetIndex() );
		}
	}
}




/////////////////////////////////////////////////////////////////////////////
// CTraktSelectListFrame

IMPLEMENT_DYNCREATE(CTraktSelectListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CTraktSelectListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CTraktSelectListFrame)
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CTraktSelectListFrame::CTraktSelectListFrame()
{
}

CTraktSelectListFrame::~CTraktSelectListFrame()
{
}

int CTraktSelectListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_bFirstOpen = TRUE;

	return 0;
}

void CTraktSelectListFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	// Save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_MARKOPLANT_TRAKTVIEWSELECT);
	SavePlacement(this, csBuf);

	CMDIChildWnd::OnClose();
}

BOOL CTraktSelectListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CTraktSelectListFrame::OnSetFocus(CWnd *pOldWnd)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

void CTraktSelectListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	// Load window placement
	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	{
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_MARKOPLANT_TRAKTVIEWSELECT);
		LoadPlacement(this, csBuf);
	}
}
