#include "stdafx.h"

#ifdef UNICODE
	#undef UNICODE
	#include "tinyxml.h"
	#define UNICODE
#else
	#include "tinyxml.h"
#endif

#include <DBBaseClass_SQLApi.h>
#include <shlobj.h>
#include <SQLAPI.h>
#include "DBFunc.h"

const TCHAR SUBDIR_HMS[]		= _T("HMS");
const TCHAR SUBDIR_DATA[]		= _T("Data");
const TCHAR SUBDIR_MARKOPLANT[]	= _T("MarkoPlant");

const TCHAR SQLCREATE_TEAM[]	= _T("CREATE TABLE [dbo].[team_table](")
								  _T("[team_num] int NOT NULL,")
								  _T("[team_region_num] int NOT NULL,")
								  _T("[team_district_num] int NOT NULL,")
								  _T("[team_name] varchar(20) NOT NULL,")
								  _T("CONSTRAINT [PK_team_table] PRIMARY KEY ")
								  _T("(")
								  _T("[team_num] ASC,")
								  _T("[team_region_num] ASC,")
								  _T("[team_district_num] ASC")
								  _T(")")
								  _T(")");

const TCHAR SQLCREATE_TRAKT[]	= _T("CREATE TABLE [dbo].[mbpl_trakt](")
								  _T("[region] int NOT NULL,")
								  _T("[distrikt] int NOT NULL,")
								  _T("[lag] int NOT NULL,")
								  _T("[karta] int NOT NULL,")
								  _T("[uppfoljning] smallint NOT NULL,")
								  _T("[invtyp] smallint NOT NULL,")
								  _T("[ursprung] smallint NOT NULL,")
								  _T("[anvandare] varchar(50) NULL,")
								  _T("[datum] datetime NOT NULL,")
								  _T("[bestand] smallint NOT NULL,")
								  _T("[delbestand] smallint NOT NULL,")
								  _T("[namn] varchar(50) NULL,")
								  _T("[areal] float NULL,")
								  _T("[hoh] smallint NOT NULL,")
								  _T("[radie] float NOT NULL,")
								  _T("[grundforhallande] smallint NULL,")
								  _T("[ytstruktur] smallint NULL,")
								  _T("[lutning] smallint NULL,")
								  _T("[vegtyp] smallint NULL,")
								  _T("[jordart] smallint NULL,")
								  _T("[markfukt] smallint NULL,")
								  _T("[uppfrysningsmark] smallint NULL,")
								  _T("[SI] varchar(5) NULL,")
								  _T("[svarighet] smallint NULL,")
								  _T("[myr] smallint NULL,")
								  _T("[hallmark] smallint NULL,")
								  _T("[omrskyddszon] smallint NULL,")
								  _T("[objskots] smallint NULL,")
								  _T("[fornminnen] smallint NULL,")
								  _T("[kultur] smallint NULL,")
								  _T("[skyddszoner] smallint NULL,")
								  _T("[naturvarde] smallint NULL,")
								  _T("[skador] smallint NULL,")
								  _T("[koordinatx] varchar(10) NULL,")
								  _T("[koordinaty] varchar(10) NULL,")
								  _T("[markpaverkan] smallint NULL,")
								  _T("[nedskrapning] smallint NULL,")
								  _T("[text] varchar(50) NULL,")
								  _T("[avgKravMbStallen] int NULL,")
								  _T("[avgKontinuerlig] float NULL,")
								  _T("[avgIntermittent] float NULL,")
								  _T("[avgPaverkadArealKont] float NULL,")
								  _T("[avgPaverkadArealInter] float NULL,")
								  _T("[avgOptimalaMbStallen] float NULL,")
								  _T("[avgBraMbStallen] float NULL,")
								  _T("[avgMarkbereddAreal] float NULL,")
								  _T("[avgOptimalBraMb] float NULL,")
								  _T("[avgRattMbMetod] float NULL,")
								  _T("[avgTall] float NULL,")
								  _T("[avgGran] float NULL,")
								  _T("[avgSfpBarr] float NULL,")
								  _T("[avgSfpLov] float NULL,")
								  _T("[avgOptimal] float NULL,")
								  _T("[avgBra] float NULL,")
								  _T("[avgOptimalBraPl] float NULL,")
								  _T("[avgUtnyttjandegrad] float NULL,")
								  _T("[avgUPI] float NULL,")
								  _T("[avgRattTradslag] float NULL,")
								  _T("[avgRattPlantantal] float NULL,")
								  _T("CONSTRAINT [PK_trakt] PRIMARY KEY ")
								  _T("(")
								  _T("[region],")
								  _T("[distrikt],")
								  _T("[lag],")
								  _T("[karta],")
								  _T("[bestand],")
								  _T("[delbestand]")
								  _T(")")
								  _T(")");

const TCHAR SQLCREATE_YTA[]		= _T("CREATE TABLE [dbo].[mbpl_yta](")
								  _T("[ytid] int IDENTITY NOT NULL,")
								  _T("[lopnr] int NOT NULL,")
								  _T("[region] int NOT NULL,")
								  _T("[distrikt] int NOT NULL,")
								  _T("[lag] int NOT NULL,")
								  _T("[karta] int NOT NULL,")
								  _T("[bestand] smallint NOT NULL,")
								  _T("[delbestand] smallint NOT NULL,")
								  _T("[koordinatx] varchar(10) NULL,")
								  _T("[koordinaty] varchar(10) NULL,")
								  _T("[grundforhallande] smallint NULL,")
								  _T("[ytstruktur] smallint NULL,")
								  _T("[lutning] smallint NULL,")
								  _T("[vegtyp] smallint NULL,")
								  _T("[jordart] smallint NOT NULL,")
								  _T("[markfukt] smallint NOT NULL,")
								  _T("[uppfrysningsmark] smallint NOT NULL,")
								  _T("[SI] varchar(50) NOT NULL,")
								  _T("[tradslag] smallint NOT NULL,")
								  _T("[markberedning] smallint NOT NULL,")
								  _T("[orsak] smallint NULL,")
								  _T("[svarighet] smallint NULL,")
								  _T("[antalspar] smallint NULL,")
								  _T("[sparbredd] smallint NULL,")
								  _T("[antalflackar] smallint NULL,")
								  _T("[arealperflack] float NULL,")
								  _T("[mbpunkt1] smallint NOT NULL,")
								  _T("[mbpunkt2] smallint NOT NULL,")
								  _T("[mbpunkt3] smallint NOT NULL,")
								  _T("[mbpunkt4] smallint NOT NULL,")
								  _T("[mbpunkt5] smallint NOT NULL,")
								  _T("[mbpunkt6] smallint NOT NULL,")
								  _T("[mbpunkt7] smallint NOT NULL,")
								  _T("[mbpunkt8] smallint NOT NULL,")
								  _T("[mbpunkt9] smallint NOT NULL,")
								  _T("[sfpbarr] smallint NOT NULL,")
								  _T("[sfplov] smallint NOT NULL,")
								  _T("[rattTradslag] smallint NOT NULL,")
								  _T("[rattMbMetod] smallint NOT NULL,")
								  _T("CONSTRAINT [PK_yta] PRIMARY KEY ")
								  _T("(")
								  _T("[ytid]")
								  _T(")")
								  _T(")");

const TCHAR SQLCREATE_PLANTA[]	= _T("CREATE TABLE [dbo].[mbpl_planta](")
								  _T("[ytid] int NOT NULL,")
								  _T("[tradslag] smallint NOT NULL,")
								  _T("[vitalitet] smallint NULL,")
								  _T("[orsak] smallint NULL,")
								  _T("[planteringspunkt] smallint NOT NULL,")
								  _T("[finnsbattre] smallint NOT NULL,")
								  _T("[planteringsdjup] smallint NOT NULL,")
								  _T("[tilltryckning] smallint NOT NULL")
								  _T(")");

const TCHAR SQLCONSTRAINT_YTA[]		= _T("ALTER TABLE [mbpl_yta] ADD CONSTRAINT [FK_mbpl_yta_mbpl_trakt] FOREIGN KEY([region], [distrikt], [lag], [karta], [bestand], [delbestand])")
									  _T("REFERENCES [mbpl_trakt] ([region], [distrikt], [lag], [karta], [bestand], [delbestand])")
									  _T("ON UPDATE CASCADE ")
									  _T("ON DELETE CASCADE");

const TCHAR SQLCONSTRAINT_PLANTA[]	= _T("ALTER TABLE[mbpl_planta] ADD CONSTRAINT [FK_mbpl_planta_mbpl_yta] FOREIGN KEY([ytid])")
									  _T("REFERENCES [mbpl_yta] ([ytid])")
									  _T("ON UPDATE CASCADE ")
									  _T("ON DELETE CASCADE");


CDBFunc::CDBFunc( DB_CONNECTION_DATA &condata )
	: CDBBaseClass_SQLApi(condata, 1)
{}

CDBFunc::~CDBFunc()
{}

void CDBFunc::setupForDBConnection( HWND hWndReciv, HWND hWndSend )
{
	// Request connection info from shell
	if (hWndReciv != NULL)
	{
		DB_CONNECTION_DATA data;
		COPYDATASTRUCT HSData;

		memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;

		data.conn = NULL;

		::SendMessage(hWndReciv, WM_COPYDATA, (WPARAM)hWndSend, (LPARAM)&HSData);
	}
}

bool CDBFunc::DeleteTeam(TEAMHEADER &th)
{
	// Remove team record from db
	try
	{
		m_saCommand.setCommandText(_T("DELETE FROM team_table WHERE team_region_num = :1 AND team_district_num = :2 AND team_num = :3"));
		m_saCommand.Param(1).setAsLong() = th.region;
		m_saCommand.Param(2).setAsLong() = th.distrikt;
		m_saCommand.Param(3).setAsLong() = th.lagnr;
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::DeletePlot(PLOTHEADER &ph)
{
	// Remove plot record from db
	try
	{
		m_saCommand.setCommandText(_T("DELETE FROM mbpl_yta WHERE ytid = :1"));
		m_saCommand.Param(1).setAsLong() = ph.ytnr;
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::DeleteTrakt(TRACTHEADER &th)
{
	// Remove trakt record from db
	try
	{
		m_saCommand.setCommandText(_T("DELETE FROM mbpl_trakt WHERE region = :1 AND distrikt = :2 AND lag = :3 AND karta = :4 AND bestand = :5 AND delbestand = :6"));
		m_saCommand.Param(1).setAsLong() = _tstoi(th.region);
		m_saCommand.Param(2).setAsLong() = _tstoi(th.distrikt);
		m_saCommand.Param(3).setAsLong() = _tstoi(th.lag);
		m_saCommand.Param(4).setAsLong() = _tstoi(th.karta);
		m_saCommand.Param(5).setAsLong() = _tstoi(th.bestand);
		m_saCommand.Param(6).setAsLong() = th.delbestand;
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetTeamData(TeamVector &teams)
{
	TEAMHEADER th;

	teams.clear();

	try
	{
		// Get all teams
		m_saCommand.setCommandText(_T("SELECT * FROM team_table"));
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			th.region	= m_saCommand.Field("team_region_num").asLong();
			th.distrikt	= m_saCommand.Field("team_district_num").asLong();
			th.lagnr	= m_saCommand.Field("team_num").asLong();
			th.lagnamn	= m_saCommand.Field("team_name").asString();
			teams.push_back(th);
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetPlotData(PlotVector &plots, int regionId /*=-1*/, int distriktId /*=-1*/, int lagId /*=-1*/, int karta /*=-1*/, int bestand /*=-1*/, int delbestand /*=-1*/)
{
	PLOTHEADER ph;
	PLANTHEADER plant;

	plots.clear();

	try
	{
		// Get all plots or plots under specified trakt
		if( regionId != -1 && distriktId != -1 && lagId != -1 && karta != -1 && bestand != -1 && delbestand != -1 )
		{
			m_saCommand.setCommandText(_T("SELECT * FROM mbpl_yta WHERE region = :1 AND distrikt = :2 AND lag = :3 AND karta = :4 AND bestand = :5 AND delbestand = :6 ORDER BY lopnr"));
			m_saCommand.Param(1).setAsLong() = regionId;
			m_saCommand.Param(2).setAsLong() = distriktId;
			m_saCommand.Param(3).setAsLong() = lagId;
			m_saCommand.Param(4).setAsLong() = karta;
			m_saCommand.Param(5).setAsLong() = bestand;
			m_saCommand.Param(6).setAsLong() = delbestand;
		}
		else
		{
			m_saCommand.setCommandText(_T("SELECT * FROM mbpl_yta ORDER BY ytid"));
		}
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			ph.ytnr			= m_saCommand.Field("ytid").asLong();
			ph.lopnr		= m_saCommand.Field("lopnr").asLong();
			ph.region		= m_saCommand.Field("region").asString();
			ph.distrikt		= m_saCommand.Field("distrikt").asString();
			ph.lag			= m_saCommand.Field("lag").asString();
			ph.karta		= m_saCommand.Field("karta").asString();
			ph.koordinatX	= m_saCommand.Field("koordinatx").asString();
			ph.koordinatY	= m_saCommand.Field("koordinaty").asString();
			ph.grundfor		= m_saCommand.Field("grundforhallande").asLong();
			ph.ytstruktur	= m_saCommand.Field("ytstruktur").asLong();
			ph.lutning		= m_saCommand.Field("lutning").asLong();
			ph.vegTyp		= m_saCommand.Field("vegtyp").asLong();
			ph.jordart		= m_saCommand.Field("jordart").asLong();
			ph.markfukt		= m_saCommand.Field("markfukt").asLong();
			ph.uppfr		= m_saCommand.Field("uppfrysningsmark").asLong();
			ph.si			= m_saCommand.Field("si").asString();
			ph.tradslag		= m_saCommand.Field("tradslag").asLong();
			ph.markberedning= m_saCommand.Field("markberedning").asLong();
			ph.orsak		= m_saCommand.Field("orsak").asLong();
			ph.svarighet	= m_saCommand.Field("svarighet").asLong();
			ph.antalSpar	= m_saCommand.Field("antalspar").asLong();
			ph.sparBredd	= m_saCommand.Field("sparbredd").asLong();
			ph.antalFlackar	= m_saCommand.Field("antalflackar").asLong();
			ph.arealPerFlack= float(m_saCommand.Field("arealperflack").asDouble());
			ph.vars[0]		= m_saCommand.Field("mbpunkt1").asLong();
			ph.vars[1]		= m_saCommand.Field("mbpunkt2").asLong();
			ph.vars[2]		= m_saCommand.Field("mbpunkt3").asLong();
			ph.vars[3]		= m_saCommand.Field("mbpunkt4").asLong();
			ph.vars[4]		= m_saCommand.Field("mbpunkt5").asLong();
			ph.vars[5]		= m_saCommand.Field("mbpunkt6").asLong();
			ph.vars[6]		= m_saCommand.Field("mbpunkt7").asLong();
			ph.vars[7]		= m_saCommand.Field("mbpunkt8").asLong();
			ph.vars[8]		= m_saCommand.Field("mbpunkt9").asLong();
			ph.sfpBarr		= m_saCommand.Field("sfpbarr").asLong();
			ph.sfpLov		= m_saCommand.Field("sfplov").asLong();
			ph.rattTradslag	= m_saCommand.Field("rattTradslag").asLong();
			ph.rattMbMetod	= m_saCommand.Field("rattMbMetod").asLong();

			plots.push_back(ph);
		}

		// Get all plants for each plot
		PlotVector::iterator iter = plots.begin();
		while( iter != plots.end() )
		{
			m_saCommand.setCommandText(_T("SELECT * FROM mbpl_planta WHERE ytid = :1"));
			m_saCommand.Param(1).setAsLong() = iter->ytnr;
			m_saCommand.Execute();
			while( m_saCommand.FetchNext() )
			{
				plant.tradslag			= m_saCommand.Field("tradslag").asLong();
				plant.vitalitet			= m_saCommand.Field("vitalitet").asLong();
				plant.orsak				= m_saCommand.Field("orsak").asLong();
				plant.planteringspunkt	= m_saCommand.Field("planteringspunkt").asLong();
				plant.battre			= m_saCommand.Field("finnsbattre").asLong();
				plant.planteringsdjup	= m_saCommand.Field("planteringsdjup").asLong();
				plant.tilltryckning		= m_saCommand.Field("tilltryckning").asLong();

				iter->plants.push_back(plant);
			}

			iter++;
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetTraktData(TractVector &tracts)
{
	int bestand;
	TRACTHEADER th;

	tracts.clear();

	try
	{
		m_saCommand.setCommandText(_T("SELECT * FROM mbpl_trakt ORDER BY karta"));
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			th.region		= m_saCommand.Field("region").asString();
			th.distrikt		= m_saCommand.Field("distrikt").asString();
			th.lag			= m_saCommand.Field("lag").asString();
			th.karta		= m_saCommand.Field("karta").asString();
			th.uppfoljning	= m_saCommand.Field("uppfoljning").asLong();
			th.invTyp		= m_saCommand.Field("invtyp").asLong();
			th.ursprung		= m_saCommand.Field("ursprung").asLong();
			th.user			= m_saCommand.Field("anvandare").asString();
			bestand			= m_saCommand.Field("bestand").asLong();
			th.delbestand	= m_saCommand.Field("delbestand").asLong();
			th.namn			= m_saCommand.Field("namn").asString();
			th.areal		= float(m_saCommand.Field("areal").asDouble());
			th.hoh			= m_saCommand.Field("hoh").asLong();
			th.radie		= float(m_saCommand.Field("radie").asDouble());
			th.grundfor		= m_saCommand.Field("grundforhallande").asLong();
			th.ytstruktur	= m_saCommand.Field("ytstruktur").asLong();
			th.lutning		= m_saCommand.Field("lutning").asLong();
			th.vegTyp		= m_saCommand.Field("vegtyp").asLong();
			th.jordart		= m_saCommand.Field("jordart").asLong();
			th.markfukt		= m_saCommand.Field("markfukt").asLong();
			th.uppfr		= m_saCommand.Field("uppfrysningsmark").asLong();
			th.si			= m_saCommand.Field("si").asString();
			th.svarighet	= m_saCommand.Field("svarighet").asLong();
			th.myr			= m_saCommand.Field("myr").asLong();
			th.hallmark		= m_saCommand.Field("hallmark").asLong();
			th.omrSkyddszon	= m_saCommand.Field("omrskyddszon").asLong();
			th.skots		= m_saCommand.Field("objskots").asLong();
			th.fornminnen	= m_saCommand.Field("fornminnen").asLong();
			th.kultur		= m_saCommand.Field("kultur").asLong();
			th.skyddszoner	= m_saCommand.Field("skyddszoner").asLong();
			th.naturvarde	= m_saCommand.Field("naturvarde").asLong();
			th.skador		= m_saCommand.Field("skador").asLong();
			th.koordinatX	= m_saCommand.Field("koordinatx").asString();
			th.koordinatY	= m_saCommand.Field("koordinaty").asString();
			th.markpaverkan	= m_saCommand.Field("markpaverkan").asLong();
			th.nedskrapning	= m_saCommand.Field("nedskrapning").asLong();
			th.text			= m_saCommand.Field("text").asString();

			// Format bestand (always four digits, may begin with a zero)
			th.bestand.Format(_T("%.4d"), bestand);

			// Format date
			SADateTime date = m_saCommand.Field(_T("datum")).asDateTime();
			th.datum.Format(_T("%04d-%02d-%02d"), date.GetYear(), date.GetMonth(), date.GetDay());

			tracts.push_back(th);
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetDistrictName(int districtId, int regionId, CString &districtName)
{
	try
	{
		m_saCommand.setCommandText(_T("SELECT district_name FROM district_table WHERE district_num = :1 AND district_region_num = :2"));
		m_saCommand.Param(1).setAsLong() = districtId;
		m_saCommand.Param(2).setAsLong() = regionId;
		m_saCommand.Execute();
		if( m_saCommand.FetchNext() )
		{
			districtName = m_saCommand.Field("district_name").asString();

			// Make sure we reach end-of-record to avoid trigging SQLAPI bug: see Redmine Wiki
			while( m_saCommand.FetchNext() ) {}
		}
		else
		{
			// Record not found
			return false;
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetRegionName(int regionId, CString &regionName)
{
	try
	{
		m_saCommand.setCommandText(_T("SELECT region_name FROM region_table WHERE region_num = :1"));
		m_saCommand.Param(1).setAsLong() = regionId;
		m_saCommand.Execute();
		if( m_saCommand.FetchNext() )
		{
			regionName = m_saCommand.Field("region_name").asString();

			// Make sure we reach end-of-record to avoid trigging SQLAPI bug: see Redmine Wiki
			while( m_saCommand.FetchNext() ) {}
		}
		else
		{
			// Record not found
			return false;
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetTeamName(int teamId, int districtId, int regionId, CString &teamName)
{
	try
	{
		m_saCommand.setCommandText(_T("SELECT team_name FROM team_table WHERE team_region_num = :1 AND team_district_num = :2 AND team_num = :3"));
		m_saCommand.Param(1).setAsLong() = regionId;
		m_saCommand.Param(2).setAsLong() = districtId;
		m_saCommand.Param(3).setAsLong() = teamId;
		m_saCommand.Execute();
		if( m_saCommand.FetchNext() )
		{
			teamName = m_saCommand.Field("team_name").asString();

			// Make sure we reach end-of-record to avoid trigging SQLAPI bug: see Redmine Wiki
			while( m_saCommand.FetchNext() ) {}
		}
		else
		{
			// Record not found
			return false;
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetDistrictList(int regionId, IdList &ids)
{
	try
	{
		// Obtain list of all district id:s
		m_saCommand.setCommandText(_T("SELECT district_num FROM district_table WHERE district_region_num = :1 ORDER BY district_num DESC"));
		m_saCommand.Param(1).setAsLong() = regionId;
		m_saCommand.Execute();

		while( m_saCommand.FetchNext() )
		{
			ids.push_back(m_saCommand.Field(1).asLong());
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetTeamList(int districtId, int regionId, IdList &ids)
{
	try
	{
		// Obtain list of all team id:s
		m_saCommand.setCommandText(_T("SELECT team_num FROM team_table WHERE team_region_num = :1 AND team_district_num = :2 ORDER BY team_num DESC"));
		m_saCommand.Param(1).setAsLong() = regionId;
		m_saCommand.Param(2).setAsLong() = districtId;
		m_saCommand.Execute();

		while( m_saCommand.FetchNext() )
		{
			ids.push_back(m_saCommand.Field(1).asLong());
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::GetRegionList(IdList &ids)
{
	try
	{
		// Obtain list of all region id:s
		m_saCommand.setCommandText(_T("SELECT region_num FROM region_table ORDER BY region_num DESC"));
		m_saCommand.Execute();

		while( m_saCommand.FetchNext() )
		{
			ids.push_back(m_saCommand.Field(1).asLong());
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::UpdateTeam(TEAMHEADER &th)
{
	try
	{
		// Update team record
		m_saCommand.setCommandText(_T("UPDATE team_table SET team_name = :4 WHERE team_region_num = :1 AND team_district_num = :2 AND team_num = :3"));
		m_saCommand.Param(1).setAsLong()	= th.region;
		m_saCommand.Param(2).setAsLong()	= th.distrikt;
		m_saCommand.Param(3).setAsLong()	= th.lagnr;
		m_saCommand.Param(4).setAsString()	= th.lagnamn;
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::UpdateTrakt(TRACTHEADER &th, int oldRegion, int oldDistrikt, int oldLag, int oldKarta, int oldBestand, int oldDelbestand)
{
	try
	{
		// Update trakt record (note: plot is automatically updated if any primary key value is changed because of the ON UPDATE CASCADE rule)
		m_saCommand.setCommandText( _T("UPDATE mbpl_trakt SET uppfoljning = :1, invtyp = :2, ursprung = :3, anvandare = :4, datum = :5, bestand = :6, delbestand = :7, namn = :8, ")
									_T("areal = :9, hoh = :10, radie = :11, grundforhallande = :12, ytstruktur = :13, lutning = :14, vegtyp = :15, jordart = :16, markfukt = :17, ")
									_T("uppfrysningsmark = :18, si = :19, svarighet = :20, myr = :21, hallmark = :22, omrskyddszon = :23, objskots = :24, fornminnen = :25, kultur = :26, ")
									_T("skyddszoner = :27, naturvarde = :28, skador = :29, koordinatx = :30, koordinaty = :31, markpaverkan = :32, nedskrapning = :33, text = :34, ")
									_T("region = :35, distrikt = :36, lag = :37, karta = :38 ")
									_T("WHERE region = :39 AND distrikt = :40 AND lag = :41 AND karta = :42 AND bestand = :43 AND delbestand = :44") );
		m_saCommand.Param(1).setAsShort()	= th.uppfoljning;
		m_saCommand.Param(2).setAsShort()	= th.invTyp;
		m_saCommand.Param(3).setAsShort()	= th.ursprung;
		m_saCommand.Param(4).setAsString()	= th.user;
		m_saCommand.Param(5).setAsString()	= th.datum;
		m_saCommand.Param(6).setAsShort()	= _tstoi(th.bestand);
		m_saCommand.Param(7).setAsShort()	= th.delbestand;
		m_saCommand.Param(8).setAsString()	= th.namn;
		m_saCommand.Param(9).setAsDouble()	= th.areal;
		m_saCommand.Param(10).setAsShort()	= th.hoh;
		m_saCommand.Param(11).setAsDouble()	= th.radie;
		m_saCommand.Param(12).setAsShort()	= th.grundfor;
		m_saCommand.Param(13).setAsShort()	= th.ytstruktur;
		m_saCommand.Param(14).setAsShort()	= th.lutning;
		m_saCommand.Param(15).setAsShort()	= th.vegTyp;
		m_saCommand.Param(16).setAsShort()	= th.jordart;
		m_saCommand.Param(17).setAsShort()	= th.markfukt;
		m_saCommand.Param(18).setAsShort()	= th.uppfr;
		m_saCommand.Param(19).setAsString()	= th.si;
		m_saCommand.Param(20).setAsShort()	= th.svarighet;
		m_saCommand.Param(21).setAsShort()	= th.myr;
		m_saCommand.Param(22).setAsShort()	= th.hallmark;
		m_saCommand.Param(23).setAsShort()	= th.omrSkyddszon;
		m_saCommand.Param(24).setAsShort()	= th.skots;
		m_saCommand.Param(25).setAsShort()	= th.fornminnen;
		m_saCommand.Param(26).setAsShort()	= th.kultur;
		m_saCommand.Param(27).setAsShort()	= th.skyddszoner;
		m_saCommand.Param(28).setAsShort()	= th.naturvarde;
		m_saCommand.Param(29).setAsShort()	= th.skador;
		m_saCommand.Param(30).setAsString()	= th.koordinatX;
		m_saCommand.Param(31).setAsString()	= th.koordinatY;
		m_saCommand.Param(32).setAsShort()	= th.markpaverkan;
		m_saCommand.Param(33).setAsShort()	= th.nedskrapning;
		m_saCommand.Param(34).setAsString()	= th.text;
		m_saCommand.Param(35).setAsLong()	= _tstoi(th.region);
		m_saCommand.Param(36).setAsLong()	= _tstoi(th.distrikt);
		m_saCommand.Param(37).setAsLong()	= _tstoi(th.lag);
		m_saCommand.Param(38).setAsLong()	= _tstoi(th.karta);
		m_saCommand.Param(39).setAsLong()	= oldRegion;
		m_saCommand.Param(40).setAsLong()	= oldDistrikt;
		m_saCommand.Param(41).setAsLong()	= oldLag;
		m_saCommand.Param(42).setAsLong()	= oldKarta;
		m_saCommand.Param(43).setAsLong()	= oldBestand;
		m_saCommand.Param(44).setAsLong()	= oldDelbestand;
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::ShowImportDialog()
{
	bool ret = true;
	const int FILELIST_SIZE = 8192;
	CFileDialog *pFileDlg;
	CString filename, filelist;
	POSITION pos;

	if( !(pFileDlg = new CFileDialog(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT, _T("XML-filer (*.xml)|*.xml||"))) )
	{
		return false;
	}

	// Show file dialog
	pFileDlg->GetOFN().lpstrFile = filelist.GetBuffer(FILELIST_SIZE);
	pFileDlg->GetOFN().nMaxFile = FILELIST_SIZE;
	if( pFileDlg->DoModal() == IDOK )
	{
		AfxGetApp()->BeginWaitCursor();

		// Import selected files to DB
		pos = pFileDlg->GetStartPosition();
		while( pos )
		{
			filename = pFileDlg->GetNextPathName(pos);
			if( ParseXml(filename) )
			{
				MoveFileToBackupDir(filename);
			}
			else
			{
				ret = false;
				break;
			}
		}

		AfxGetApp()->EndWaitCursor();
	}
	else
	{
		ret = false;
	}

	delete pFileDlg;

	return ret;
}

bool CDBFunc::AddTeam(TEAMHEADER &th)
{
	try
	{
		// Add team record
		m_saCommand.setCommandText(_T("INSERT INTO team_table (team_region_num, team_district_num, team_num, team_name) VALUES(:1, :2, :3, :4)"));
		m_saCommand.Param(1).setAsLong()	= th.region;
		m_saCommand.Param(2).setAsLong()	= th.distrikt;
		m_saCommand.Param(3).setAsLong()	= th.lagnr;
		m_saCommand.Param(4).setAsString()	= th.lagnamn;
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::AddRecord(TRACTHEADER &th)
{
	CString date;
	int plotid = 0;

	FormatDate(th.datum, date);

	// Turn off autocommit in case an error occur
	getDBConnection().conn->setAutoCommit(SA_AutoCommitOff);

	try
	{
		// Make sure this file hasn't been imported already
		m_saCommand.setCommandText( _T("SELECT karta FROM mbpl_trakt WHERE region = :1 AND distrikt = :2 AND lag = :3 AND karta = :4 AND bestand = :5 AND delbestand = :6") );
		m_saCommand.Param(1).setAsLong()	= _tstoi(th.region);
		m_saCommand.Param(2).setAsLong()	= _tstoi(th.distrikt);
		m_saCommand.Param(3).setAsLong()	= _tstoi(th.lag);
		m_saCommand.Param(4).setAsLong()	= _tstoi(th.karta);
		m_saCommand.Param(5).setAsShort()	= _tstoi(th.bestand);
		m_saCommand.Param(6).setAsShort()	= th.delbestand;
		m_saCommand.Execute();
		if( m_saCommand.FetchNext() )
		{
			// Make sure we reach end-of-record to avoid trigging SQLAPI bug: see Redmine Wiki
			while( m_saCommand.FetchNext() ) {}

			// Record found
			CString str;
			str.Format( _T("Denna trakt finns redan inl�st!\nRegion/distrikt/lag/karta: %s/%s/%s/%s\nBest�nd-delbest�nd: %s-%d"),
						th.region, th.distrikt, th.lag, th.karta, th.bestand, th.delbestand );
			AfxMessageBox(str);
			return false;
		}

		// Add tract record
		m_saCommand.setCommandText( _T("INSERT INTO mbpl_trakt (region, distrikt, lag, karta, uppfoljning, invtyp, ursprung, anvandare, datum, bestand, delbestand, namn, areal, hoh, radie, grundforhallande, ytstruktur, lutning, vegtyp, jordart, markfukt, uppfrysningsmark, si, svarighet, myr, hallmark, omrskyddszon, objskots, fornminnen, kultur, skyddszoner, naturvarde, skador, koordinatx, koordinaty, markpaverkan, nedskrapning, text) ")
									_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37, :38)") );
		m_saCommand.Param(1).setAsLong()	= _tstoi(th.region);
		m_saCommand.Param(2).setAsLong()	= _tstoi(th.distrikt);
		m_saCommand.Param(3).setAsLong()	= _tstoi(th.lag);
		m_saCommand.Param(4).setAsLong()	= _tstoi(th.karta);
		m_saCommand.Param(5).setAsShort()	= th.uppfoljning;
		m_saCommand.Param(6).setAsShort()	= th.invTyp;
		m_saCommand.Param(7).setAsShort()	= th.ursprung;
		m_saCommand.Param(8).setAsString()	= th.user;
		m_saCommand.Param(9).setAsString()	= date;
		m_saCommand.Param(10).setAsShort()	= _tstoi(th.bestand);
		m_saCommand.Param(11).setAsShort()	= th.delbestand;
		m_saCommand.Param(12).setAsString()	= th.namn;
		m_saCommand.Param(13).setAsDouble()	= th.areal;
		m_saCommand.Param(14).setAsShort()	= th.hoh;
		m_saCommand.Param(15).setAsDouble()	= th.radie;
		m_saCommand.Param(16).setAsShort()	= th.grundfor;
		m_saCommand.Param(17).setAsShort()	= th.ytstruktur;
		m_saCommand.Param(18).setAsShort()	= th.lutning;
		m_saCommand.Param(19).setAsShort()	= th.vegTyp;
		m_saCommand.Param(20).setAsShort()	= th.jordart;
		m_saCommand.Param(21).setAsShort()	= th.markfukt;
		m_saCommand.Param(22).setAsShort()	= th.uppfr;
		m_saCommand.Param(23).setAsString()	= th.si;
		m_saCommand.Param(24).setAsShort()	= th.svarighet;
		m_saCommand.Param(25).setAsShort()	= th.myr;
		m_saCommand.Param(26).setAsShort()	= th.hallmark;
		m_saCommand.Param(27).setAsShort()	= th.omrSkyddszon;
		m_saCommand.Param(28).setAsShort()	= th.skots;
		m_saCommand.Param(29).setAsShort()	= th.fornminnen;
		m_saCommand.Param(30).setAsShort()	= th.kultur;
		m_saCommand.Param(31).setAsShort()	= th.skyddszoner;
		m_saCommand.Param(32).setAsShort()	= th.naturvarde;
		m_saCommand.Param(33).setAsShort()	= th.skador;
		m_saCommand.Param(34).setAsString()	= th.koordinatX;
		m_saCommand.Param(35).setAsString()	= th.koordinatY;
		m_saCommand.Param(36).setAsShort()	= th.markpaverkan;
		m_saCommand.Param(37).setAsShort()	= th.nedskrapning;
		m_saCommand.Param(38).setAsString()	= th.text;
		m_saCommand.Execute();

		// Add all plots under this tract
		PlotVector::const_iterator iter = th.plots.begin();
		while( iter != th.plots.end() )
		{
			m_saCommand.setCommandText( _T("INSERT INTO mbpl_yta (region, distrikt, lag, karta, bestand, delbestand, koordinatx, koordinaty, grundforhallande, ytstruktur, lutning, vegtyp, jordart, markfukt, uppfrysningsmark, si, tradslag, markberedning, orsak, svarighet, antalspar, sparbredd, antalflackar, arealperflack, mbpunkt1, mbpunkt2, mbpunkt3, mbpunkt4, mbpunkt5, mbpunkt6, mbpunkt7, mbpunkt8, mbpunkt9, sfpbarr, sfplov, rattTradslag, rattMbMetod, lopnr) ")
										_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37, :38)") );
			m_saCommand.Param(1).setAsLong()	= _tstoi(th.region);
			m_saCommand.Param(2).setAsLong()	= _tstoi(th.distrikt);
			m_saCommand.Param(3).setAsLong()	= _tstoi(th.lag);
			m_saCommand.Param(4).setAsLong()	= _tstoi(th.karta);
			m_saCommand.Param(5).setAsLong()	= _tstoi(th.bestand);
			m_saCommand.Param(6).setAsLong()	= th.delbestand;
			m_saCommand.Param(7).setAsString()	= iter->koordinatX;
			m_saCommand.Param(8).setAsString()	= iter->koordinatY;
			m_saCommand.Param(9).setAsLong()	= iter->grundfor;
			m_saCommand.Param(10).setAsLong()	= iter->ytstruktur;
			m_saCommand.Param(11).setAsLong()	= iter->lutning;
			m_saCommand.Param(12).setAsLong()	= iter->vegTyp;
			m_saCommand.Param(13).setAsLong()	= iter->jordart;
			m_saCommand.Param(14).setAsLong()	= iter->markfukt;
			m_saCommand.Param(15).setAsLong()	= iter->uppfr;
			m_saCommand.Param(16).setAsString()	= iter->si;
			m_saCommand.Param(17).setAsLong()	= iter->tradslag;
			m_saCommand.Param(18).setAsLong()	= iter->markberedning;
			m_saCommand.Param(19).setAsLong()	= iter->orsak;
			m_saCommand.Param(20).setAsLong()	= iter->svarighet;
			m_saCommand.Param(21).setAsLong()	= iter->antalSpar;
			m_saCommand.Param(22).setAsLong()	= iter->sparBredd;
			m_saCommand.Param(23).setAsLong()	= iter->antalFlackar;
			m_saCommand.Param(24).setAsDouble()	= iter->arealPerFlack;
			m_saCommand.Param(25).setAsLong()	= iter->vars[0];
			m_saCommand.Param(26).setAsLong()	= iter->vars[1];
			m_saCommand.Param(27).setAsLong()	= iter->vars[2];
			m_saCommand.Param(28).setAsLong()	= iter->vars[3];
			m_saCommand.Param(29).setAsLong()	= iter->vars[4];
			m_saCommand.Param(30).setAsLong()	= iter->vars[5];
			m_saCommand.Param(31).setAsLong()	= iter->vars[6];
			m_saCommand.Param(32).setAsLong()	= iter->vars[7];
			m_saCommand.Param(33).setAsLong()	= iter->vars[8];
			m_saCommand.Param(34).setAsLong()	= iter->sfpBarr;
			m_saCommand.Param(35).setAsLong()	= iter->sfpLov;
			m_saCommand.Param(36).setAsLong()	= iter->rattTradslag;
			m_saCommand.Param(37).setAsLong()	= iter->rattMbMetod;
			m_saCommand.Param(38).setAsLong()	= iter->lopnr;
			m_saCommand.Execute();

			// Get id of current plot
			m_saCommand.setCommandText(_T("SELECT MAX(ytid) FROM mbpl_yta"));
			m_saCommand.Execute();
			while( m_saCommand.FetchNext() )
			{
				plotid = m_saCommand.Field(1).asLong();
			}

			// Add all plants under this plot
			PlantVector::const_iterator iter2 = iter->plants.begin();
			while( iter2 != iter->plants.end() )
			{
				m_saCommand.setCommandText( _T("INSERT INTO mbpl_planta (ytid, tradslag, vitalitet, orsak, planteringspunkt, finnsbattre, planteringsdjup, tilltryckning) ")
											_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8)") );
				m_saCommand.Param(1).setAsLong() = plotid;
				m_saCommand.Param(2).setAsLong() = iter2->tradslag;
				if( iter2->tradslag % 10 == 1 ) // Kolla om tr�dslag �r d�d och s�tt vitalitet
				{
					m_saCommand.Param(3).setAsLong() = 3; // D�d
				}
				else
				{
					m_saCommand.Param(3).setAsLong() = iter2->vitalitet; // God/gul
				}
				m_saCommand.Param(4).setAsLong() = iter2->orsak;
				m_saCommand.Param(5).setAsLong() = iter2->planteringspunkt;
				m_saCommand.Param(6).setAsLong() = iter2->battre;
				m_saCommand.Param(7).setAsLong() = iter2->planteringsdjup;
				m_saCommand.Param(8).setAsLong() = iter2->tilltryckning;
				m_saCommand.Execute();

				iter2++;
			}

			iter++;
		}

		// Everything succeeded - commit changes to db
		getDBConnection().conn->Commit();

		// Keep track of last added record
		m_lastTractKey.region		= th.region;
		m_lastTractKey.distrikt		= th.distrikt;
		m_lastTractKey.lag			= th.lag.IsEmpty() ? _T("0") : th.lag;	// Team might be missing in file
		m_lastTractKey.karta		= th.karta;
		m_lastTractKey.bestand.Format(_T("%.4d"), _tstoi(th.bestand));		// Always four digits, pad with zeros
		m_lastTractKey.delbestand	= th.delbestand;
	}
	catch( SAException &e )
	{
		// Undo any changes to db
		try
		{
			getDBConnection().conn->Rollback();
		}
		catch(...)
		{
		}

		AfxMessageBox( e.ErrText() );
		return false;
	}

	// Turn autocommit back on
	getDBConnection().conn->setAutoCommit(SA_AutoCommitOn);

	return true;
}

bool CDBFunc::CalculateRecord(TRACTHEADER &th)
{
	float sumMbSvarighet[3] = { 0, 0, 0 };
	float sumAntalSpar = 0;
	float sumSparbredd = 0;
	float sumAntalFlackar = 0;
	float sumArealPerFlack = 0;
	float sumProvyteAreal;
	float sumMbPunkt0 = 0;
	float sumMbPunkt1 = 0;
	float sumMbPunkt2 = 0;
	float sumMbProvytor = 0;
	float sumTall = 0;
	float sumGran = 0;
	float sumSfpBarr = 0;
	float sumSfpLov = 0;
	float sumPlanteringspunkt0 = 0;
	float sumPlanteringspunkt12 = 0;
	float sumFinnsBattre = 0;
	float sumBraPlDjup = 0;
	float sumBraTilltryckning = 0;
	float sumRattTradslag = 0;
	float sumRattMbMetod = 0;
	float sumGodkandaYtor = 0;

	float avgKontinuerlig = 0;
	float avgIntermittent = 0;
	float avgPaverkadArealKont = 0;
	float avgPaverkadArealInter = 0;
	float avgOptimalaMbStallen = 0;
	float avgBraMbStallen = 0;
	float avgMarkbereddAreal = 0;
	float avgOptimalBraMb = 0;
	float avgTall = 0;
	float avgGran = 0;
	float avgSfpBarr = 0;
	float avgSfpLov = 0;
	float avgOptimalPl = 0;
	float avgBraPl = 0;
	float avgOptimalBraPl = 0;
	float avgUtnyttjandegrad = 0;
	float avgUPI = 0;
	float avgRattTradslag = 0;
	float avgRattMbMetod = 0;
	float avgRattPlantantal = 0;

	float numPlots = float(th.plots.size());
	float numPlants = 0;
	float ytAreal;

	int kravMbStallen = 1; // Set to 1 to avoid division by 0 in case anything goes wrong

	int si = _tstoi(th.si.Right(th.si.GetLength() - 1));
	if( si <= 18 )		// - 18
	{
		kravMbStallen = 2100;
	}
	else if( si == 19 )	// 19
	{
		kravMbStallen = 2200;
	}
	else if( si < 23 )	// 20 - 22
	{
		kravMbStallen = 2300;
	}
	else if( si == 23 )	// 23
	{
		kravMbStallen = 2400;
	}
	else if( si < 29 )	// 24 - 28
	{
		kravMbStallen = 2500;
	}
	else if( si == 29 )	// 29
	{
		kravMbStallen = 2600;
	}
	else if( si >= 30 )	// 30 +
	{
		kravMbStallen = 2700;
	}

	// Om markberedningssv�righet bed�mt till Sv�r (= 3) reduceras kravet p� antalet plantor med 500 per hektar. G�ller b�de mb och pl.
	if( th.svarighet == 3 )
	{
		kravMbStallen -= 500;
	}

	// Summa provyteareal (provyteradie^2 * pi * "antal provytor")
	ytAreal = th.radie * th.radie * 3.1415926536f;
	sumProvyteAreal = ytAreal * numPlots;
	if( sumProvyteAreal == 0 ) sumProvyteAreal = 0.000001f; // Just in case, to avoid divide by zero


	// Sum up required values
	PlotVector::const_iterator iter1 = th.plots.begin();
	while( iter1 != th.plots.end() )
	{
		// MB sv�righeter
		if( iter1->svarighet >= 0 && iter1->svarighet < 3 )
		{
			sumMbSvarighet[iter1->svarighet]++;
		}

		// Kont. MB - antal sp�r
		if( iter1->antalSpar > 0 )
		{
			sumAntalSpar += iter1->antalSpar;
		}

		// Kont. MB - sp�rbredd
		if( iter1->sparBredd > 0 )
		{
			sumSparbredd += iter1->sparBredd;
		}

		// Intermittent MB - antal fl�ckar
		if( iter1->antalFlackar > 0 )
		{
			sumAntalFlackar += iter1->antalFlackar;
		}

		// Intermittent MB - areal per fl�ck
		if( iter1->arealPerFlack > 0 )
		{
			sumArealPerFlack += iter1->arealPerFlack;
		}

		// Summa MB punkt 0
		if( iter1->vars[0] > 0 )
		{
			sumMbPunkt0 += iter1->vars[0];
		}
		// Summa MB punkt 1
		if( iter1->vars[1] > 0 )
		{
			sumMbPunkt1 += iter1->vars[1];
		}
		// Summa MB punkt 2
		if( iter1->vars[2] > 0 )
		{
			sumMbPunkt2 += iter1->vars[2];
		}

		// Antal MB provytor
		if( iter1->markberedning >= 0 && iter1->markberedning < 3 )
		{
			sumMbProvytor++;
		}

		// Summa r�tt MB metod (1 = Ja)
		if( iter1->rattMbMetod == 1 )
		{
			sumRattMbMetod++;
		}

		// Summa sj�lvf�ryngrande plantor (endast barr)
		if( iter1->sfpBarr > 0 )
		{
			sumSfpBarr += iter1->sfpBarr;
		}
		if( iter1->sfpLov > 0 )
		{
			sumSfpLov += iter1->sfpLov;
		}

		// Summa r�tt tr�dslag (1 = Ja)
		if( iter1->rattTradslag == 1 )
		{
			sumRattTradslag++;
		}

		// Godk�nd yta? (r�tt antal plantor, r�kna per ha)
		float plantCt = (float(iter1->plants.size()) + max(iter1->sfpBarr, 0)) / ytAreal * 10000.0f;
		if( plantCt / kravMbStallen >= 0.8f && plantCt / kravMbStallen <= 1.2f )
		{
			sumGodkandaYtor++;
		}

		// Summera fr�n plantor
		PlantVector::const_iterator iter2 = iter1->plants.begin();
		while( iter2 != iter1->plants.end() )
		{
			// Summa tall (inkludera �ven contorta)
			if( iter2->tradslag == 10 || iter2->tradslag == 11 || iter2->tradslag == 60 || iter2->tradslag == 61 )
			{
				sumTall++;
			}

			// Summa gran
			if( iter2->tradslag == 20 || iter2->tradslag == 21 )
			{
				sumGran++;
			}

			// Summa planteringspunkt = 0
			if( iter2->planteringspunkt == 0 )
			{
				sumPlanteringspunkt0++;
			}

			// Summa planteringspunkt = 1 + 2
			if( iter2->planteringspunkt == 1 || iter2->planteringspunkt == 2 )
			{
				sumPlanteringspunkt12++;
			}

			// Summa "Finns b�ttre" (1 = Ja)
			if( iter2->battre == 1 )
			{
				sumFinnsBattre++;
			}

			// Summa underk�nda plantor; ("Finns b�ttre" (= "Ja") eller "Planteringsdjup" (= "F�r djup" eller "F�r grund") eller "Tilltryckning" = ("F�r l�s" eller "F�r h�rd"))
			/*if( iter2->battre == 1 || iter2->planteringsdjup == 2 || iter2->planteringsdjup == 3 || iter2->tilltryckning == 2 || iter2->tilltryckning == 3 )
			{
				sumUnderkandaPlantor++;
			}*/

			// Summa "Bra planteringsdjup" (pl.djup = 1)
			if( iter2->planteringsdjup == 1 )
			{
				sumBraPlDjup++;
			}

			// Summa "Bra planteringspunkt" (pl.punkt = 1)
			if( iter2->tilltryckning == 1 )
			{
				sumBraTilltryckning++;
			}

			iter2++;
		}

		numPlants += int(iter1->plants.size());

		iter1++;
	}

	// Calculate values - markberedning
	if( th.uppfoljning != 1 ) // 1 = Endast plantinventering
	{
		if( sumAntalSpar > 0 )
		{
			avgKontinuerlig			= 1.0f / ((th.radie * 2.0f * numPlots) / sumAntalSpar) * 10000.0f;							// [M/ha]	1/((Radien * 2 * antal ytor) / (Summa "Antal rader")) * 10 000
		}
		else
		{
			avgKontinuerlig			= 0;
		}
		if( sumProvyteAreal > 0.0f )
		{
			avgIntermittent			= (sumAntalFlackar / sumProvyteAreal) * 10000.0f;											// [St/ha]	Medel "Antal fl�ckar" / provytearea * 10 000
		}
		else
		{
			avgIntermittent			= 0;
		}
		if( numPlots > 0 )
		{
			avgPaverkadArealKont	= avgKontinuerlig * (sumSparbredd / numPlots) / 1000.0f;									// [%]		Kont. M/ha * medel Sp�rbredd / 1000
			avgPaverkadArealInter	= avgIntermittent * (sumArealPerFlack / numPlots) / 100.0f;									// [%]		Intermittent "Antal fl�ckar" / ha * medeltal f�r "Areal per fl�ck" / 100
			avgMarkbereddAreal		= sumMbProvytor / numPlots * 100.0f;														// [%]		Antal MB provytor / Totalt antal provytor (inkl ej MB) * 100
			avgRattMbMetod			= sumRattMbMetod / numPlots * 100.0f;														// [%]		Andel ytor med r�tt mb metod * 100
		}
		else
		{
			avgPaverkadArealKont	= 0;
			avgPaverkadArealInter	= 0;
			avgMarkbereddAreal		= 0;
		}
		if( sumProvyteAreal > 0.0f )
		{
			avgOptimalaMbStallen		= sumMbPunkt0 / sumProvyteAreal * 10000.0f;												// [St/ha]	Summa MB punkt 1 / Summa provyteareal * 10 000
			avgBraMbStallen				= (sumMbPunkt1 + sumMbPunkt2) / sumProvyteAreal * 10000.0f;								// [St/ha]	(Summa MB punkt 1 + Summa MB punkt 2) / Summa provyteareal * 10 000
		}
		else
		{
			avgOptimalaMbStallen		= 0;
			avgBraMbStallen				= 0;
		}

		avgOptimalBraMb					= (avgOptimalaMbStallen + avgBraMbStallen) / kravMbStallen * 100.0f;					// [%]		("Optimal + Bra MB st�llen") / "Krav mb st�llen" * 100
	}

	// Plantering
	if( numPlots > 0 )
	{
		avgRattPlantantal			= ((sumTall + sumGran + sumSfpBarr) / sumProvyteAreal * 10000.0f) / kravMbStallen * 100;	// [%]		Summa (Tall + Gran + SFPBarr) / "Krav mb st�llen" * 100; �ven SFPBarr r�knas d�rf�r kan numPlants vara noll.
		avgRattTradslag				= sumRattTradslag / numPlots * 100;															// [%]		Summa "R�tt tr�dslag" / Antal provytor * 100
	}
	else
	{
		avgRattPlantantal			= 0;
		avgRattTradslag				= 0;
	}
	if( numPlants > 0 )
	{
		if( sumProvyteAreal > 0.0f )
		{
			avgTall					= sumTall / sumProvyteAreal * 10000.0f;														// [St/ha]	Summa Tall plantor / Summa provyteareal * 10 000
			avgGran					= sumGran / sumProvyteAreal * 10000.0f;														// [St/ha]	Summa Gran plantor / Summa provyteareal * 10 000
			avgSfpBarr				= sumSfpBarr / sumProvyteAreal * 10000.0f;													// [St/ha]	Summa Sj�lvf�ryngrande plantor (barr) / Summa provyteareal * 10 000
			avgSfpLov				= sumSfpLov / sumProvyteAreal * 10000.0f;													// [St/ha]	Summa Sj�lvf�ryngrande plantor (l�v) / Summa provyteareal * 10 000
			avgOptimalPl			= sumPlanteringspunkt0 / sumProvyteAreal * 10000.0f;										// [St/ha]	Summa Planteringspunkt = 0 / Summa provyteareal * 10 000
			avgBraPl				= sumPlanteringspunkt12 / sumProvyteAreal * 10000.0f;										// [St/ha]	Summa Planteringspunkt = 1 + 2 / Summa provyteareal * 10 000
			avgOptimalBraPl			= (avgOptimalPl + avgBraPl) / (numPlants / sumProvyteAreal * 10000.0f) * 100;				// [%]		(Summa optimala + bra pl. punkter per ha) / "Totalt planterat T + G per ha" * 100
		}
		else
		{
			avgTall					= 0;
			avgGran					= 0;
			avgSfpBarr				= 0;
			avgSfpLov				= 0;
			avgOptimalPl			= 0;
			avgBraPl				= 0;
			avgOptimalBraPl			= 0;
		}
		avgUtnyttjandegrad		= (1.0f - (sumFinnsBattre / numPlants)) * 100;													// [%]		(1 - (Summa "Finns b�ttre" / "Totalt planterat T + G")) * 100
		avgUPI					= (avgRattPlantantal + avgUtnyttjandegrad +
								  ((sumBraPlDjup / numPlants * 100) + (sumBraTilltryckning / numPlants * 100)) / 2.0f) / 3.0f;	// [%]		("R�tt antal plantor" + "Utnyttjandegrad MB" + (Summa "Bra pl. djup" * 100 + Summa "Bra tilltryckning" * 100) / 2) / 3
	}

	// Save values to db
	try
	{
		m_saCommand.setCommandText( _T("UPDATE mbpl_trakt SET avgKravMbStallen = :1, avgKontinuerlig = :2, avgIntermittent = :3, avgPaverkadArealKont = :4, ")
									_T("avgPaverkadArealInter = :5, avgOptimalaMbStallen = :6, avgBraMbStallen = :7, avgMarkbereddAreal = :8, avgOptimalBraMb = :9, avgRattMbMetod = :10, ")
									_T("avgTall = :11, avgGran = :12, avgSfpBarr = :13, avgSfpLov = :14, avgOptimal = :15, avgBra = :16, avgOptimalBraPl = :17, avgUtnyttjandegrad = :18, ")
									_T("avgUPI = :19, avgRattTradslag = :20, avgRattPlantantal = :21 ")
									_T("WHERE region = :22 AND distrikt = :23 AND lag = :24 AND karta = :25 AND bestand = :26 AND delbestand = :27") );
		m_saCommand.Param(1).setAsLong() = long(kravMbStallen);
		m_saCommand.Param(2).setAsDouble() = avgKontinuerlig;
		m_saCommand.Param(3).setAsDouble() = avgIntermittent;
		m_saCommand.Param(4).setAsDouble() = avgPaverkadArealKont;
		m_saCommand.Param(5).setAsDouble() = avgPaverkadArealInter;
		m_saCommand.Param(6).setAsDouble() = avgOptimalaMbStallen;
		m_saCommand.Param(7).setAsDouble() = avgBraMbStallen;
		m_saCommand.Param(8).setAsDouble() = avgMarkbereddAreal;
		m_saCommand.Param(9).setAsDouble() = avgOptimalBraMb;
		m_saCommand.Param(10).setAsDouble() = avgRattMbMetod;
		m_saCommand.Param(11).setAsDouble() = avgTall;
		m_saCommand.Param(12).setAsDouble() = avgGran;
		m_saCommand.Param(13).setAsDouble() = avgSfpBarr;
		m_saCommand.Param(14).setAsDouble() = avgSfpLov;
		m_saCommand.Param(15).setAsDouble() = avgOptimalPl;
		m_saCommand.Param(16).setAsDouble() = avgBraPl;
		m_saCommand.Param(17).setAsDouble() = avgOptimalBraPl;
		m_saCommand.Param(18).setAsDouble() = avgUtnyttjandegrad;
		m_saCommand.Param(19).setAsDouble() = avgUPI;
		m_saCommand.Param(20).setAsDouble() = avgRattTradslag;
		m_saCommand.Param(21).setAsDouble() = avgRattPlantantal;
		m_saCommand.Param(22).setAsLong() = _tstoi(th.region);
		m_saCommand.Param(23).setAsLong() = _tstoi(th.distrikt);
		m_saCommand.Param(24).setAsLong() = _tstoi(th.lag);
		m_saCommand.Param(25).setAsLong() = _tstoi(th.karta);
		m_saCommand.Param(26).setAsLong() = _tstoi(th.bestand);
		m_saCommand.Param(27).setAsLong() = th.delbestand;
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::ExportRecords(CString dbFile, CString conditions)
{
	SAConnection dbConn;
	SACommand dbCommand;
	SAString connStr;
	SAString str, date;

	AfxGetApp()->BeginWaitCursor();

	try
	{
		connStr.Format(_T("Driver={Microsoft Access Driver (*.mdb)};Dbq=%s;Uid=Admin;Pwd=;"), dbFile.GetString());
		dbConn.Connect(connStr, "", "", SA_ODBC_Client);
		dbCommand.setConnection(&dbConn);

		// Try creating required tables (we cannot check if they already exist so this will throw an exception if it's already created)
		try
		{
			dbCommand.setCommandText( SQLCREATE_TRAKT );
			dbCommand.Execute();
		}
		catch( ... )
		{
		}

		try
		{
			SAString tmp(SQLCREATE_YTA);
			tmp.Replace(_T("IDENTITY"), _T(""));
			dbCommand.setCommandText( tmp );
			dbCommand.Execute();
		}
		catch( ... )
		{
		}

		try
		{
			dbCommand.setCommandText( SQLCREATE_PLANTA );
			dbCommand.Execute();
		}
		catch( ... )
		{
		}

		// Copy data - trakter
		str = _T("SELECT * FROM mbpl_trakt");
		if( !conditions.IsEmpty() ) str += _T(" WHERE ") + conditions;
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			// Format date
			SADateTime dt = m_saCommand.Field("datum").asDateTime();
			date.Format(_T("%04d-%02d-%02d"), dt.GetYear(), dt.GetMonth(), dt.GetDay());

			// Insert trakt data
			dbCommand.setCommandText( _T("INSERT INTO mbpl_trakt (region, distrikt, lag, karta, uppfoljning, invtyp, ursprung, anvandare, datum, bestand, delbestand, namn, areal, hoh, radie, grundforhallande, ytstruktur, lutning, vegtyp, jordart, markfukt, uppfrysningsmark, si, svarighet, myr, hallmark, omrskyddszon, objskots, fornminnen, kultur, skyddszoner, naturvarde, skador, koordinatx, koordinaty, markpaverkan, nedskrapning, [text]) ")
									  _T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37, :38)") );
			dbCommand.Param(1).setAsLong()		= m_saCommand.Field("region").asLong();
			dbCommand.Param(2).setAsLong()		= m_saCommand.Field("distrikt").asLong();
			dbCommand.Param(3).setAsLong()		= m_saCommand.Field("lag").asLong();
			dbCommand.Param(4).setAsLong()		= m_saCommand.Field("karta").asLong();
			dbCommand.Param(5).setAsLong()		= m_saCommand.Field("uppfoljning").asLong();
			dbCommand.Param(6).setAsLong()		= m_saCommand.Field("invtyp").asLong();
			dbCommand.Param(7).setAsLong()		= m_saCommand.Field("ursprung").asLong();
			dbCommand.Param(8).setAsString()	= m_saCommand.Field("anvandare").asString();
			dbCommand.Param(9).setAsString()	= date;
			dbCommand.Param(10).setAsLong()		= m_saCommand.Field("bestand").asLong();
			dbCommand.Param(11).setAsLong()		= m_saCommand.Field("delbestand").asLong();
			dbCommand.Param(12).setAsString()	= m_saCommand.Field("namn").asString();
			dbCommand.Param(13).setAsDouble()	= m_saCommand.Field("areal").asDouble();
			dbCommand.Param(14).setAsLong()		= m_saCommand.Field("hoh").asLong();
			dbCommand.Param(15).setAsDouble()	= m_saCommand.Field("radie").asDouble();
			dbCommand.Param(16).setAsLong()		= m_saCommand.Field("grundforhallande").asLong();
			dbCommand.Param(17).setAsLong()		= m_saCommand.Field("ytstruktur").asLong();
			dbCommand.Param(18).setAsLong()		= m_saCommand.Field("lutning").asLong();
			dbCommand.Param(19).setAsLong()		= m_saCommand.Field("vegtyp").asLong();
			dbCommand.Param(20).setAsLong()		= m_saCommand.Field("jordart").asLong();
			dbCommand.Param(21).setAsLong()		= m_saCommand.Field("markfukt").asLong();
			dbCommand.Param(22).setAsLong()		= m_saCommand.Field("uppfrysningsmark").asLong();
			dbCommand.Param(23).setAsString()	= m_saCommand.Field("si").asString();
			dbCommand.Param(24).setAsLong()		= m_saCommand.Field("svarighet").asLong();
			dbCommand.Param(25).setAsLong()		= m_saCommand.Field("myr").asLong();
			dbCommand.Param(26).setAsLong()		= m_saCommand.Field("hallmark").asLong();
			dbCommand.Param(27).setAsLong()		= m_saCommand.Field("omrskyddszon").asLong();
			dbCommand.Param(28).setAsLong()		= m_saCommand.Field("objskots").asLong();
			dbCommand.Param(29).setAsLong()		= m_saCommand.Field("fornminnen").asLong();
			dbCommand.Param(30).setAsLong()		= m_saCommand.Field("kultur").asLong();
			dbCommand.Param(31).setAsLong()		= m_saCommand.Field("skyddszoner").asLong();
			dbCommand.Param(32).setAsLong()		= m_saCommand.Field("naturvarde").asLong();
			dbCommand.Param(33).setAsLong()		= m_saCommand.Field("skador").asLong();
			dbCommand.Param(34).setAsString()	= m_saCommand.Field("koordinatx").asString();
			dbCommand.Param(35).setAsString()	= m_saCommand.Field("koordinaty").asString();
			dbCommand.Param(36).setAsLong()		= m_saCommand.Field("markpaverkan").asLong();
			dbCommand.Param(37).setAsLong()		= m_saCommand.Field("nedskrapning").asLong();
			dbCommand.Param(38).setAsString()	= m_saCommand.Field("text").asString();
			dbCommand.Execute();

			// For some reason we may get a crash when inserting all parameters at once so we complete with average values here after the insert
			dbCommand.setCommandText( _T("UPDATE mbpl_trakt SET avgKravMbStallen = :1, avgKontinuerlig = :2, avgIntermittent = :3, avgPaverkadArealKont = :4, avgPaverkadArealInter = :5, avgOptimalaMbStallen = :6, avgBraMbStallen = :7, avgMarkbereddAreal = :8, avgOptimalBraMb = :9, avgRattMbMetod = :10, avgTall = :11, avgGran = :12, avgSfpBarr = :13, avgSfpLov = :14, avgOptimal = :15, avgBra = :16, avgOptimalBraPl = :17, avgUtnyttjandegrad = :18, avgUPI = :19, avgRattTradslag = :20, avgRattPlantantal = :21 ")
									  _T("WHERE region = :22 AND distrikt = :23 AND lag = :24 AND karta = :25 AND bestand = :26 AND delbestand = :27") );
			dbCommand.Param(1).setAsLong()		= m_saCommand.Field("avgKravMbStallen").asLong();
			dbCommand.Param(2).setAsDouble()	= m_saCommand.Field("avgKontinuerlig").asDouble();
			dbCommand.Param(3).setAsDouble()	= m_saCommand.Field("avgIntermittent").asDouble();
			dbCommand.Param(4).setAsDouble()	= m_saCommand.Field("avgPaverkadArealKont").asDouble();
			dbCommand.Param(5).setAsDouble()	= m_saCommand.Field("avgPaverkadArealInter").asDouble();
			dbCommand.Param(6).setAsDouble()	= m_saCommand.Field("avgOptimalaMbStallen").asDouble();
			dbCommand.Param(7).setAsDouble()	= m_saCommand.Field("avgBraMbStallen").asDouble();
			dbCommand.Param(8).setAsDouble()	= m_saCommand.Field("avgMarkbereddAreal").asDouble();
			dbCommand.Param(9).setAsDouble()	= m_saCommand.Field("avgOptimalBraMb").asDouble();
			dbCommand.Param(10).setAsDouble()	= m_saCommand.Field("avgRattMbMetod").asDouble();
			dbCommand.Param(11).setAsDouble()	= m_saCommand.Field("avgTall").asDouble();
			dbCommand.Param(12).setAsDouble()	= m_saCommand.Field("avgGran").asDouble();
			dbCommand.Param(13).setAsDouble()	= m_saCommand.Field("avgSfpBarr").asDouble();
			dbCommand.Param(14).setAsDouble()	= m_saCommand.Field("avgSfpLov").asDouble();
			dbCommand.Param(15).setAsDouble()	= m_saCommand.Field("avgOptimal").asDouble();
			dbCommand.Param(16).setAsDouble()	= m_saCommand.Field("avgBra").asDouble();
			dbCommand.Param(17).setAsDouble()	= m_saCommand.Field("avgOptimalBraPl").asDouble();
			dbCommand.Param(18).setAsDouble()	= m_saCommand.Field("avgUtnyttjandegrad").asDouble();
			dbCommand.Param(19).setAsDouble()	= m_saCommand.Field("avgUPI").asDouble();
			dbCommand.Param(20).setAsDouble()	= m_saCommand.Field("avgRattTradslag").asDouble();
			dbCommand.Param(21).setAsDouble()	= m_saCommand.Field("avgRattPlantantal").asDouble();
			dbCommand.Param(22).setAsLong()		= m_saCommand.Field("region").asLong();
			dbCommand.Param(23).setAsLong()		= m_saCommand.Field("distrikt").asLong();
			dbCommand.Param(24).setAsLong()		= m_saCommand.Field("lag").asLong();
			dbCommand.Param(25).setAsLong()		= m_saCommand.Field("karta").asLong();
			dbCommand.Param(26).setAsLong()		= m_saCommand.Field("bestand").asLong();
			dbCommand.Param(27).setAsLong()		= m_saCommand.Field("delbestand").asLong();
			dbCommand.Execute();
		}

		// Ytor
		//SELECT * FROM mbpl_yta WHERE EXISTS(SELECT 1 FROM mbpl_trakt WHERE mbpl_trakt.region = mbpl_yta.region AND mbpl_trakt.distrikt = mbpl_yta.distrikt AND mbpl_trakt.lag = mbpl_yta.lag AND mbpl_trakt.karta = mbpl_yta.karta /*AND conditions*/)
		m_saCommand.setCommandText(_T("SELECT * FROM mbpl_yta"));
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			dbCommand.setCommandText( _T("INSERT INTO mbpl_yta (region, distrikt, lag, karta, bestand, delbestand, koordinatx, koordinaty, grundforhallande, ytstruktur, lutning, vegtyp, jordart, markfukt, uppfrysningsmark, si, tradslag, markberedning, orsak, svarighet, antalspar, sparbredd, antalflackar, arealperflack, mbpunkt1, mbpunkt2, mbpunkt3, mbpunkt4, mbpunkt5, mbpunkt6, mbpunkt7, mbpunkt8, mbpunkt9, sfpbarr, sfplov, rattTradslag, rattMbMetod, ytid, lopnr) ")
									  _T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37, :38, :39)") );
			dbCommand.Param(1).setAsLong()		= m_saCommand.Field("region").asLong();
			dbCommand.Param(2).setAsLong()		= m_saCommand.Field("distrikt").asLong();
			dbCommand.Param(3).setAsLong()		= m_saCommand.Field("lag").asLong();
			dbCommand.Param(4).setAsLong()		= m_saCommand.Field("karta").asLong();
			dbCommand.Param(5).setAsLong()		= m_saCommand.Field("bestand").asLong();
			dbCommand.Param(6).setAsLong()		= m_saCommand.Field("delbestand").asLong();
			dbCommand.Param(7).setAsString()	= m_saCommand.Field("koordinatx").asString();
			dbCommand.Param(8).setAsString()	= m_saCommand.Field("koordinaty").asString();
			dbCommand.Param(9).setAsLong()		= m_saCommand.Field("grundforhallande").asLong();
			dbCommand.Param(10).setAsLong()		= m_saCommand.Field("ytstruktur").asLong();
			dbCommand.Param(11).setAsLong()		= m_saCommand.Field("lutning").asLong();
			dbCommand.Param(12).setAsLong()		= m_saCommand.Field("vegtyp").asLong();
			dbCommand.Param(13).setAsLong()		= m_saCommand.Field("jordart").asLong();
			dbCommand.Param(14).setAsLong()		= m_saCommand.Field("markfukt").asLong();
			dbCommand.Param(15).setAsLong()		= m_saCommand.Field("uppfrysningsmark").asLong();
			dbCommand.Param(16).setAsString()	= m_saCommand.Field("si").asString();
			dbCommand.Param(17).setAsLong()		= m_saCommand.Field("tradslag").asLong();
			dbCommand.Param(18).setAsLong()		= m_saCommand.Field("markberedning").asLong();
			dbCommand.Param(19).setAsLong()		= m_saCommand.Field("orsak").asLong();
			dbCommand.Param(20).setAsLong()		= m_saCommand.Field("svarighet").asLong();
			dbCommand.Param(21).setAsLong()		= m_saCommand.Field("antalspar").asLong();
			dbCommand.Param(22).setAsLong()		= m_saCommand.Field("sparbredd").asLong();
			dbCommand.Param(23).setAsLong()		= m_saCommand.Field("antalflackar").asLong();
			dbCommand.Param(24).setAsDouble()	= m_saCommand.Field("arealperflack").asDouble();
			dbCommand.Param(25).setAsLong()		= m_saCommand.Field("mbpunkt1").asLong();
			dbCommand.Param(26).setAsLong()		= m_saCommand.Field("mbpunkt2").asLong();
			dbCommand.Param(27).setAsLong()		= m_saCommand.Field("mbpunkt3").asLong();
			dbCommand.Param(28).setAsLong()		= m_saCommand.Field("mbpunkt4").asLong();
			dbCommand.Param(29).setAsLong()		= m_saCommand.Field("mbpunkt5").asLong();
			dbCommand.Param(30).setAsLong()		= m_saCommand.Field("mbpunkt6").asLong();
			dbCommand.Param(31).setAsLong()		= m_saCommand.Field("mbpunkt7").asLong();
			dbCommand.Param(32).setAsLong()		= m_saCommand.Field("mbpunkt8").asLong();
			dbCommand.Param(33).setAsLong()		= m_saCommand.Field("mbpunkt9").asLong();
			dbCommand.Param(34).setAsLong()		= m_saCommand.Field("sfpbarr").asLong();
			dbCommand.Param(35).setAsLong()		= m_saCommand.Field("sfplov").asLong();
			dbCommand.Param(36).setAsLong()		= m_saCommand.Field("ratttradslag").asLong();
			dbCommand.Param(37).setAsLong()		= m_saCommand.Field("rattmbmetod").asLong();
			dbCommand.Param(38).setAsLong()		= m_saCommand.Field("ytid").asLong();
			dbCommand.Param(39).setAsLong()		= m_saCommand.Field("lopnr").asLong();
			dbCommand.Execute();
		}

		// Plantor
		//SELECT * FROM mbpl_planta WHERE EXISTS(SELECT 1 FROM mbpl_yta WHERE EXISTS(SELECT 1 FROM mbpl_trakt WHERE mbpl_trakt.region = mbpl_yta.region AND mbpl_trakt.distrikt = mbpl_yta.distrikt AND mbpl_trakt.lag = mbpl_yta.lag AND mbpl_trakt.karta = mbpl_yta.karta /*AND conditions*/) AND mbpl_planta.ytid = mbpl_yta.ytid)
		m_saCommand.setCommandText(_T("SELECT * FROM mbpl_planta"));
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			dbCommand.setCommandText( _T("INSERT INTO mbpl_planta (ytid, tradslag, vitalitet, orsak, planteringspunkt, finnsbattre, planteringsdjup, tilltryckning) ")
									  _T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8)") );
			dbCommand.Param(1).setAsLong() = m_saCommand.Field("ytid").asLong();
			dbCommand.Param(2).setAsLong() = m_saCommand.Field("tradslag").asLong();
			dbCommand.Param(3).setAsLong() = m_saCommand.Field("vitalitet").asLong();
			dbCommand.Param(4).setAsLong() = m_saCommand.Field("orsak").asLong();
			dbCommand.Param(5).setAsLong() = m_saCommand.Field("planteringspunkt").asLong();
			dbCommand.Param(6).setAsLong() = m_saCommand.Field("finnsbattre").asLong();
			dbCommand.Param(7).setAsLong() = m_saCommand.Field("planteringsdjup").asLong();
			dbCommand.Param(8).setAsLong() = m_saCommand.Field("tilltryckning").asLong();
			dbCommand.Execute();
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	// Make sure temp connection is close before leaving this function
	try
	{
		dbConn.Disconnect();
	}
	catch( ... )
	{
	}

	AfxGetApp()->EndWaitCursor();

	return true;
}

bool CDBFunc::MakeSureTablesExist()
{
	SAString str;

	try
	{
		// Lag
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'team_table' AND xtype='U') %s"), SQLCREATE_TEAM);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// Trakt
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'mbpl_trakt' AND xtype='U') %s"), SQLCREATE_TRAKT);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// Yta
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'mbpl_yta' AND xtype='U') %s"), SQLCREATE_YTA);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// Planta
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'mbpl_planta' AND xtype='U') %s"), SQLCREATE_PLANTA);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();


		// Foreign key constraints
		// Yta - Trakt
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM Information_Schema.Table_Constraints WHERE CONSTRAINT_NAME = 'FK_mbpl_yta_mbpl_trakt') %s"), SQLCONSTRAINT_YTA);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// Planta - Yta
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM Information_Schema.Table_Constraints WHERE CONSTRAINT_NAME = 'FK_mbpl_planta_mbpl_yta') %s"), SQLCONSTRAINT_PLANTA);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::MoveFileToBackupDir(CString &filepath)
{
	// Set up backup directory if not exist
	CString path;
	TCHAR szPath[MAX_PATH];
	if( SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, NULL, 0, szPath)) )
	{
		path.Format( _T("%s\\%s"), szPath, SUBDIR_HMS );
	}
	else
	{
		return false;
	}

	// Create directory %My documents%\HMS
	if( !CreateDirectory(path, NULL) )
	{
		if( GetLastError() != ERROR_ALREADY_EXISTS )
		{
			return false;
		}
	}

	// Create directory %My documents%\HMS\Data
	path += CString("\\") + SUBDIR_DATA;
	if( !CreateDirectory(path, NULL) )
	{
		if( GetLastError() != ERROR_ALREADY_EXISTS )
		{
			return false;
		}
	}

	// Create directory %My documents%\HMS\Data\MarkoPlant
	path += CString("\\") + SUBDIR_MARKOPLANT;
	if( !CreateDirectory(path, NULL) )
	{
		if( GetLastError() != ERROR_ALREADY_EXISTS )
		{
			return false;
		}
	}

	// Move file to backup dir (append index in case filename already exist; "filename(x).ext")
	CString filename = filepath.Right(filepath.GetLength() - filepath.ReverseFind('\\') - 1);
	CString moveto = path + "\\" + filename;
	CString movetoex = moveto;
	CString filenum;
	int i = 1;
	while( !(MoveFileEx(filepath, movetoex, MOVEFILE_COPY_ALLOWED | MOVEFILE_WRITE_THROUGH)) ) // Enable move from network device
	{
		// Only run this loop in case we need to change the filename
		if( GetLastError() != ERROR_ALREADY_EXISTS )
		{
			CString str; str.Format(_T("Misslyckades att flytta '%s' till '%s'!"), filename, movetoex);
			AfxMessageBox(str);
			break;
		}

		filenum.Format( _T("(%d)"), i++ );
		movetoex = moveto;
		movetoex.Insert( movetoex.ReverseFind('.'), filenum );
	}

	return true;
}

bool CDBFunc::ParseXml(CString &filename)
{
	USES_CONVERSION;
	TiXmlNode *pNode, *pRootNode;
#ifdef UNICODE
	TiXmlDocument doc( W2A(filename) );
#else
	TiXmlDocument doc( filename );
#endif

	// Load xml file
	if( !doc.LoadFile() )
	{
		return false;
	}

	// Get root node
	pRootNode = doc.FirstChild("XML");
	if( !pRootNode )
	{
		return false;
	}

	// Find all 'Trakt' elements
	pNode = pRootNode->FirstChild("Trakt");
	while( pNode )
	{
		CString file;
		TCHAR fn[260], ext[260];
		_tsplitpath(filename, NULL, NULL, fn, ext);
		file.Format(_T("%s%s"), fn, ext);
		if( !ParseTrakt(pNode, file) )
		{
			return false;
		}
		pNode = pNode->NextSibling();
	}

	return true;
}

bool CDBFunc::ParseTrakt(TiXmlNode *pNode, CString &filename)
{
	int plotnum = 0; // Counter for plots under in this tract
	TRACTHEADER th;
	TiXmlElement *pElem;

	pElem = pNode->FirstChildElement();
	if( !pElem )
	{
		return false;
	}
	while( pElem )
	{
		// Get all elements under each 'Yta'
		if( strcmp(pElem->Value(), "Yta") == 0 )
		{
			if( !ParseYta(pElem, th, ++plotnum) )
				return false;
		}
		else
		{
			// Store values (use lower-case to make case-insensitive)
			char szValue[256]; strcpy(szValue, pElem->Value()); _strlwr(szValue);
			if( strcmp(szValue, "best�ndsnamn") == 0 )
			{
				// MbPl - wrong application for this file
				CString str; str.Format(_T("Denna trakt '%s' tillh�r R�jning och kan inte l�sas in till MbPl!"), filename);
				AfxMessageBox(str);
				return false;
			}
			else if( strcmp(szValue, "uppf�ljning") == 0 )
			{
				th.uppfoljning = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "invtyp") == 0 )
			{
				th.invTyp = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "ursprung") == 0 )
			{
				th.ursprung = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "region") == 0 )
			{
				th.region = pElem->GetText();
			}
			else if( strcmp(szValue, "distrikt") == 0 )
			{
				th.distrikt = pElem->GetText();
			}
			else if( strcmp(szValue, "lag") == 0 )
			{
				th.lag = pElem->GetText();
			}
			else if( strcmp(szValue, "user") == 0 )
			{
				th.user = pElem->GetText();
			}
			else if( strcmp(szValue, "datum") == 0 )
			{
				th.datum = pElem->GetText();
			}
			else if( strcmp(szValue, "karta") == 0 )
			{
				th.karta = pElem->GetText();
			}
			else if( strcmp(szValue, "best�nd") == 0 )
			{
				th.bestand = pElem->GetText();
				if( th.bestand.IsEmpty() ) th.bestand = "-1";
			}
			else if( strcmp(szValue, "delbest�nd") == 0 )
			{
				th.delbestand = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "namn") == 0 )
			{
				th.namn = pElem->GetText();
			}
			else if( strcmp(szValue, "areal") == 0 )
			{
				CString str = pElem->GetText();
				str.Replace('.', ',');
				th.areal = float(_tstof(str));
			}
			else if( strcmp(szValue, "hoh") == 0 )
			{
				th.hoh = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "radie") == 0 )
			{
				CString str = pElem->GetText();
				str.Replace('.', ',');
				th.radie = float(_tstof(str));
			}
			else if( strcmp(szValue, "grundf�rh�llande") == 0 )
			{
				th.grundfor = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "ytstruktur") == 0 )
			{
				th.ytstruktur = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "lutning") == 0 )
			{
				th.lutning = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "vegtyp") == 0 )
			{
				th.vegTyp = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "jordart") == 0 )
			{
				th.jordart = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "markfukt") == 0 )
			{
				th.markfukt = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "uppfrysningsmark") == 0 )
			{
				th.uppfr = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "si") == 0 )
			{
				th.si = pElem->GetText();
			}
			else if( strcmp(szValue, "sv�righet") == 0 )
			{
				th.svarighet = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "myr") == 0 )
			{
				th.myr = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "h�llmark") == 0 )
			{
				th.hallmark = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "omrskyddszon") == 0 )
			{
				th.omrSkyddszon = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "objsk�ts") == 0 )
			{
				th.skots = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "fornminnen") == 0 )
			{
				th.fornminnen = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "kultur") == 0 )
			{
				th.kultur = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "skyddszoner") == 0 )
			{
				th.skyddszoner = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "naturv�rde") == 0 )
			{
				th.naturvarde = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "skador") == 0 )
			{
				th.skador = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "koordinatx") == 0 )
			{
				th.koordinatX = pElem->GetText();
			}
			else if( strcmp(szValue, "koordinaty") == 0 )
			{
				th.koordinatY = pElem->GetText();
			}
			else if( strcmp(szValue, "markp�verkan") == 0 )
			{
				th.markpaverkan = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "nedskr�pning") == 0 )
			{
				th.nedskrapning = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "text") == 0 )
			{
				th.text = pElem->GetText();
			}
		}

		pElem = pElem->NextSiblingElement();
	}

	// S�tt best�nd, delbest�nd till 0 om ej angivet (ing�r i prim�rnyckeln)
	if( _tstoi(th.bestand) < 0 )
	{
		th.bestand = "0";
	}
	if( th.delbestand < 0 )
	{
		th.delbestand = 0;
	}

	AddRecord(th);
	CalculateRecord(th);

	return true;
}

bool CDBFunc::ParseYta(TiXmlNode *pNode, TRACTHEADER &th, int plotnum)
{
	int ytid = 0;
	PLOTHEADER ph;
	TiXmlElement *pElem;

	ph.lopnr = plotnum;

	// Get all elements under this node
	pElem = pNode->FirstChildElement();
	if( !pElem )
	{
		return false;
	}
	while( pElem )
	{
		// Get all elements under each 'Planta'
		if( strcmp(pElem->Value(), "Planta") == 0 )
		{
			if( !ParsePlanta(pElem, ph) )
				return false;
		}
		else
		{
			// Store values (use lower-case to make case-insensitive)
			char szValue[256]; strcpy(szValue, pElem->Value()); _strlwr(szValue);
			if( strcmp(szValue, "koordinatx") == 0 )
			{
				ph.koordinatX = pElem->GetText();
			}
			else if( strcmp(szValue, "koordinaty") == 0 )
			{
				ph.koordinatY = pElem->GetText();
			}
			else if( strcmp(szValue, "grundf�rh�llande") == 0 )
			{
				ph.grundfor = atoi(pElem->GetText()); 
			}
			else if( strcmp(szValue, "ytstruktur") == 0 )
			{
				ph.ytstruktur = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "lutning") == 0 )
			{
				ph.lutning = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "vegtyp") == 0 )
			{
				ph.vegTyp = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "jordart") == 0 )
			{
				ph.jordart = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "markfukt") == 0 )
			{
				ph.markfukt = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "uppfrysningsmark") == 0 )
			{
				ph.uppfr = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "si") == 0 )
			{
				ph.si = pElem->GetText();
			}
			else if( strcmp(szValue, "tr�dslag") == 0 )
			{
				ph.tradslag = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "markberedning") == 0 )
			{
				ph.markberedning = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "orsak") == 0 )
			{
				ph.orsak = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "sv�righet") == 0 )
			{
				ph.svarighet = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "antalsp�r") == 0 )
			{
				ph.antalSpar = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "sp�rbredd") == 0 )
			{
				ph.sparBredd = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "antalfl�ckar") == 0 )
			{
				ph.antalFlackar = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "antalfl�ckar") == 0 )
			{
				ph.antalFlackar = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "arealperfl�ck") == 0 )
			{
				CString str = pElem->GetText();
				str.Replace('.', ',');
				ph.arealPerFlack = float(_tstof(str));
			}
			else if( strcmp(szValue, "mbpunkt1") == 0 )
			{
				ph.vars[0] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "mbpunkt2") == 0 )
			{
				ph.vars[1] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "mbpunkt3") == 0 )
			{
				ph.vars[2] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "mbpunkt4") == 0 )
			{
				ph.vars[3] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "mbpunkt5") == 0 )
			{
				ph.vars[4] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "mbpunkt6") == 0 )
			{
				ph.vars[5] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "mbpunkt7") == 0 )
			{
				ph.vars[6] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "mbpunkt8") == 0 )
			{
				ph.vars[7] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "mbpunkt9") == 0 )
			{
				ph.vars[8] = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "sfpbarr") == 0 )
			{
				ph.sfpBarr = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "sfpl�v") == 0 )
			{
				ph.sfpLov = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "r�tttr�dslag") == 0 )
			{
				ph.rattTradslag = atoi(pElem->GetText());
			}
			else if( strcmp(szValue, "r�ttmbmetod") == 0 )
			{
				ph.rattMbMetod = atoi(pElem->GetText());
			}
		}

		pElem = pElem->NextSiblingElement();
	}

	th.plots.push_back(ph);

	return true;
}

bool CDBFunc::ParsePlanta(TiXmlNode *pNode, PLOTHEADER &ph)
{
	PLANTHEADER plant;
	TiXmlElement *pElem;

	// Get all elements under this node
	pElem = pNode->FirstChildElement();
	if( !pElem )
	{
		return false;
	}
	while( pElem )
	{
		// Store values (use lower-case to make case-insensitive)
		char szValue[256]; strcpy(szValue, pElem->Value()); _strlwr(szValue);
		if( strcmp(szValue, "tr�dslag") == 0 )
		{
			plant.tradslag = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "vitalitet") == 0 )
		{
			plant.vitalitet = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "orsak") == 0 )
		{
			plant.orsak = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "planteringspunkt") == 0 )
		{
			plant.planteringspunkt = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "finnsb�ttre") == 0 )
		{
			plant.battre = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "planteringsdjup") == 0 )
		{
			plant.planteringsdjup = atoi(pElem->GetText());
		}
		else if( strcmp(szValue, "tilltryckning") == 0 )
		{
			plant.tilltryckning = atoi(pElem->GetText());
		}

		pElem = pElem->NextSiblingElement();
	}

	ph.plants.push_back(plant);

	return true;
}

// Helper function used to format date string from yyyymmdd to yyyy-mm-dd
bool CDBFunc::FormatDate(CString str, CString &date)
{
	TCHAR y[5], m[3], d[3];

	if( _tcslen(str) == 10 && str[4] == '-' && str[7] == '-' )
	{
		// String is already formatted
		date = str;
		return true;
	}

	if( _tcslen(str) != 8 )
	{
		AfxMessageBox(_T("Ok�nt datumformat. Ange datum p� formen ����mmdd."));
		return false;
	}

	// Extract date parts
	memcpy( y, (TCHAR*)(str.GetBuffer() + 0), 4 * sizeof(TCHAR) ); y[4] = '\0';
	memcpy( m, (TCHAR*)(str.GetBuffer() + 4), 2 * sizeof(TCHAR) ); m[2] = '\0';
	memcpy( d, (TCHAR*)(str.GetBuffer() + 6), 2 * sizeof(TCHAR) ); d[2] = '\0';

	date.Format( _T("%s-%s-%s"), y, m, d );

	return true;
}
