// UMMarkoPlant.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "ExportData.h"
#include "TeamFrame.h"
#include "TeamView.h"
#include "TeamSelectList.h"
#include "TraktFrame.h"
#include "TraktView.h"
#include "TraktSelectList.h"
#include "PlotView.h"
#include "PlotFrame.h"
#include "PlotSelectList.h"
#include "UMMarkoPlantDoc.h"
#include "UMMarkoPlant.h"

/////////////////////////////////////////////////////////////////////////////
// Initialization of MFC Extension DLL

#include "afxdllx.h"    // standard MFC Extension DLL routines


// globals
RLFReader* g_pXML;
HINSTANCE g_hInstance = NULL;
static AFX_EXTENSION_MODULE UMMarkoPlantDLL = { NULL, NULL };


extern "C" int APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UMMarkoPlant.DLL Initializing!\n");
	
		// create the XML-parser class.
		g_pXML = new RLFReader();

		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMMarkoPlantDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UMMarkoPlant.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMMarkoPlantDLL);

		delete g_pXML;
	}

	g_hInstance = hInstance;

	return 1;   // ok
}


void DLL_BUILD InitModule(CWinApp *app, LPCTSTR suite, vecINDEX_TABLE &idx, vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(UMMarkoPlantDLL);
	CString sLangFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;

	CString sModuleFN = getModuleFN(g_hInstance);
	sLangFN.Format(_T("%s%s"), getLanguageDir(), PROGRAM_NAME);

	CWinApp* pApp = AfxGetApp();

	// Doc templates
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMTEAM, 
			RUNTIME_CLASS(CMarkoPlantDoc),
			RUNTIME_CLASS(CTeamFrame),
			RUNTIME_CLASS(CTeamView)));
	idx.push_back(INDEX_TABLE(IDD_FORMTEAM, suite, sLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMTRAKT, 
			RUNTIME_CLASS(CMarkoPlantDoc),
			RUNTIME_CLASS(CTraktFrame),
			RUNTIME_CLASS(CTraktView)));
	idx.push_back(INDEX_TABLE(IDD_FORMTRAKT, suite, sLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMPLOT, 
			RUNTIME_CLASS(CMarkoPlantDoc),
			RUNTIME_CLASS(CPlotFrame),
			RUNTIME_CLASS(CPlotView)));
	idx.push_back(INDEX_TABLE(IDD_FORMPLOT, suite, sLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_TEAMSELECT, 
			RUNTIME_CLASS(CMarkoPlantDoc),
			RUNTIME_CLASS(CTeamSelectListFrame),
			RUNTIME_CLASS(CTeamSelectList)));
	idx.push_back(INDEX_TABLE(IDD_TEAMSELECT, suite, sLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_TRAKTSELECT, 
			RUNTIME_CLASS(CMarkoPlantDoc),
			RUNTIME_CLASS(CTraktSelectListFrame),
			RUNTIME_CLASS(CTraktSelectList)));
	idx.push_back(INDEX_TABLE(IDD_TRAKTSELECT, suite, sLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_PLOTSELECT, 
			RUNTIME_CLASS(CMarkoPlantDoc),
			RUNTIME_CLASS(CPlotSelectListFrame),
			RUNTIME_CLASS(CPlotSelectList)));
	idx.push_back(INDEX_TABLE(IDD_PLOTSELECT, suite, sLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_EXPORT, 
			RUNTIME_CLASS(CMarkoPlantDoc),
			RUNTIME_CLASS(CExportDataFrame),
			RUNTIME_CLASS(CExportDataView)));
	idx.push_back(INDEX_TABLE(IDD_EXPORT, suite, sLangFN, TRUE));

	
	// Get version information; 060803 p�d
	sVersion	= getVersionInfo(g_hInstance,VER_NUMBER);
	sCopyright	= getVersionInfo(g_hInstance,VER_COPYRIGHT);
	sCompany	= getVersionInfo(g_hInstance,VER_COMPANY);

	vecInfo.push_back(INFO_TABLE(-999,
		2/* Set to 1 to indicate a SUITE; 2 indicates a  User Module*/,
		sLangFN.GetBuffer(),
		sVersion.GetBuffer(),
		sCopyright.GetBuffer(),
		sCompany.GetBuffer()));
}
