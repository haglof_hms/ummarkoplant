#pragma once

void showFormView(int idd, LPCTSTR lang_fn);
CView *getFormViewByID(int idd);
BOOL messageDialog(LPCTSTR cap, LPCTSTR ok_btn, LPCTSTR cancel_btn, LPCTSTR msg);
