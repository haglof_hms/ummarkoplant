#pragma once

#include "DBFunc.h"


// CExportData form view

class CTeamView : public CFormView
{
	DECLARE_DYNCREATE(CTeamView)

protected:
	CTeamView();
	virtual ~CTeamView();

	void BeginAdding();
	void EndAdding();
	void DeleteTeam();
	void SaveTeam();

	void ListDistricts(int regionId);
	void ReloadTeamData(bool bDataAdded = false);

	bool m_bAdding;
	CDBFunc *m_pDB;
	int m_nDBIndex;
	TeamVector m_teamData;

public:
	void PopulateData(int idx = -1);
	void SetupToolbarButtons();

	virtual void OnInitialUpdate();

// Dialog Data
	enum { IDD = IDD_FORMTEAM };

protected:
	CMyComboBox m_region;
	CMyComboBox m_distrikt;
	CMyExtEdit m_lagnr;
	CMyExtEdit m_lagnamn;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);

	afx_msg void OnCbnSelchangeRegion();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
};
