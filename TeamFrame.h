#pragma once

///////////////////////////////////////////////////////////////////////////////////////////
// CTeamFrame

class CTeamFrame : public CXTPFrameWndBase<CMDIChildWnd>
{
	DECLARE_DYNCREATE(CTeamFrame)

public:
	CTeamFrame();
	virtual ~CTeamFrame();

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	BOOL m_bFirstOpen;
	CString m_sLangFN;

	afx_msg void OnClose();
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	DECLARE_MESSAGE_MAP()

	LRESULT OnMessageFromShell(WPARAM wParam, LPARAM lParam);
};
