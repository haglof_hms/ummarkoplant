#include "stdafx.h"
#include "UMMarkoPlantGenerics.h"
#include "TraktFrame.h"
#include "TraktView.h"


/////////////////////////////////////////////////////////////////////////////
// CTraktFrame

IMPLEMENT_DYNCREATE(CTraktFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CTraktFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CTraktFrame)
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_SHOWWINDOW()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTraktFrame construction/destruction

CTraktFrame::CTraktFrame():
m_bOnce(true)
{
}

CTraktFrame::~CTraktFrame()
{
}

void CTraktFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 070905 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	// Save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_MARKOPLANT_TRAKTVIEW);
	SavePlacement(this, csBuf);
	m_bOnce = true;

	CMDIChildWnd::OnClose();
}

int CTraktFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CMDIChildWnd::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Set up language filename
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);

	return 0;
}

void CTraktFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CMDIChildWnd::OnShowWindow(bShow, nStatus);

	// Load window position
    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false; // Important: set this flag before loading window placement!

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_MARKOPLANT_TRAKTVIEW);
		LoadPlacement(this, csBuf);
    }
}

void CTraktFrame::OnSetFocus(CWnd *pOldWnd)
{
	// Let form view set up toolbar buttons
	CTraktView *pView = (CTraktView*)getFormViewByID( IDD_FORMTRAKT );
	if( pView )
	{
		pView->SetupToolbarButtons();
	}
}

BOOL CTraktFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

LRESULT CTraktFrame::OnMessageFromShell(WPARAM wParam, LPARAM lParam)
{
	switch( wParam )
	{
	case ID_DBNAVIG_LIST:
		showFormView( IDD_TRAKTSELECT, m_sLangFN );
		break;

	default:
		// Forward message to view(s)
		CDocument *pDoc = GetActiveDocument();
		if( pDoc )
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while( pos )
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage( MSG_IN_SUITE, wParam, lParam );
			}
		}
		break;
	}

	return 0L;
}
