#include "stdafx.h"
#include "DBFunc.h"
#include "UMMarkoPlantGenerics.h"
#include "UMMarkoPlant.h"
#include "TeamSelectList.h"
#include "TeamView.h"


/////////////////////////////////////////////////////////////////////////////
// CTeamSelectList

IMPLEMENT_DYNCREATE(CTeamSelectList, CFormView)

BEGIN_MESSAGE_MAP(CTeamSelectList, CFormView)
	//{{AFX_MSG_MAP(CTeamSelectList)
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY(NM_CLICK, IDC_TEAMREPORT, OnReportItemClick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


CTeamSelectList::CTeamSelectList():
CFormView(CTeamSelectList::IDD),
m_pDB(NULL)
{
}

CTeamSelectList::~CTeamSelectList()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CTeamSelectList::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

void CTeamSelectList::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	SetupReport();
}

void CTeamSelectList::PopulateReport()
{
	CXTPReportRecord *pRec;
	TeamVector teamData;

	// Clear report
	m_report.ClearReport();

	// Get data from db
	if( m_pDB )
	{
		m_pDB->GetTeamData( teamData );
	}

	// Add new data
	for( UINT i = 0; i < teamData.size(); i++ )
	{
		// Add new record, set up subitems
		pRec = new CXTPReportRecord;

		//m_pDB->GetRegionName(teamData[i].region, str);
		pRec->AddItem( new CXTPReportRecordItemNumber(teamData[i].region) );

		//m_pDB->GetDistrictName(teamData[i].distrikt, teamData[i].region, str);
		pRec->AddItem( new CXTPReportRecordItemNumber(teamData[i].distrikt) );

		pRec->AddItem( new CXTPReportRecordItemNumber(teamData[i].lagnr) );
		pRec->AddItem( new CXTPReportRecordItemText(teamData[i].lagnamn) );
		m_report.AddRecord( pRec );
	}

	m_report.Populate();
	m_report.UpdateWindow();
}

bool CTeamSelectList::SetupReport()
{
	CXTPReportColumn *pCol;

	// Create list view if not already done
	if( m_report.GetSafeHwnd() == NULL )
	{
		if( !m_report.Create(this, IDC_TEAMREPORT) )
		{
			return false;
		}
	}

	// Set up columns
	m_report.ShowWindow( SW_NORMAL );

	pCol = m_report.AddColumn( new CXTPReportColumn(0, _T("Region"), 50) );
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );

	pCol = m_report.AddColumn( new CXTPReportColumn(1, _T("Distrikt"), 50) );
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );

	pCol = m_report.AddColumn( new CXTPReportColumn(2, _T("Lagnr"), 50) );
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );

	pCol = m_report.AddColumn( new CXTPReportColumn(3, _T("Lagnamn"), 50) );
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );

	OnSize(0,0,0);

	PopulateReport();

	return true;
}


// CTeamSelectList message handlers

BOOL CTeamSelectList::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}
	}

	return CFormView::OnCopyData(pWnd, pData);
}

int CTeamSelectList::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CFormView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	return 0;
}

void CTeamSelectList::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	// Adjust report control size
	if( m_report.GetSafeHwnd() )
	{
		setResize( &m_report, 0, 0, rect.right, rect.bottom );
	}
}

void CTeamSelectList::OnReportItemClick(NMHDR *pNotifyStruct, LRESULT *pResult)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if( pItemNotify->pRow )
	{
		// Update team view
		CXTPReportRecord *pRec = (CXTPReportRecord*)pItemNotify->pItem->GetRecord();
		CTeamView *pView = (CTeamView*)getFormViewByID(IDD_FORMTEAM);
		if( pView )
		{
			pView->PopulateData( pRec->GetIndex() );
		}
	}
}




/////////////////////////////////////////////////////////////////////////////
// CTeamSelectListFrame

IMPLEMENT_DYNCREATE(CTeamSelectListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CTeamSelectListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CTeamSelectListFrame)
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CTeamSelectListFrame::CTeamSelectListFrame()
{
}

CTeamSelectListFrame::~CTeamSelectListFrame()
{
}

int CTeamSelectListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_bFirstOpen = TRUE;

	return 0;
}

void CTeamSelectListFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	// Save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_MARKOPLANT_TEAMVIEWSELECT);
	SavePlacement(this, csBuf);

	CMDIChildWnd::OnClose();
}

BOOL CTeamSelectListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CTeamSelectListFrame::OnSetFocus(CWnd *pOldWnd)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

void CTeamSelectListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	// Load window placement
	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	{
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_MARKOPLANT_TEAMVIEWSELECT);
		LoadPlacement(this, csBuf);
	}
}
