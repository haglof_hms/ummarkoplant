#pragma once

#include "stdafx.h"

class CMarkoPlantDoc : public CDocument
{
protected: // create from serialization only
	CMarkoPlantDoc();
	virtual ~CMarkoPlantDoc();

	DECLARE_DYNCREATE(CMarkoPlantDoc)

// Generated message map functions
protected:
	//{{AFX_MSG(CMarkoPlantDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
