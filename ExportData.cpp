// PlotSelectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DBFunc.h"
#include "UMMarkoPlant.h"
#include "ExportData.h"


/////////////////////////////////////////////////////////////////////////////
// CExportDataView

IMPLEMENT_DYNCREATE(CExportDataView, CFormView)

BEGIN_MESSAGE_MAP(CExportDataView, CFormView)
	//{{AFX_MSG_MAP(CExportDataView)
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


CExportDataView::CExportDataView():
CFormView(CExportDataView::IDD),
m_pDB(NULL)
{
}

CExportDataView::~CExportDataView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CExportDataView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

void CExportDataView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));
}

void CExportDataView::ShowExportDialog()
{
	CFileDialog dlg(FALSE, _T("*.mdb"), NULL, 0, _T("Access-databaser (*.mdb)|*.mdb|Alla filer (*.*)|*.*||"));

	// Show export dialog
	if( dlg.DoModal() == IDOK )
	{
		if( m_pDB->ExportRecords(dlg.GetPathName(), _T("")) )
		{
			AfxMessageBox(_T("Exporteringen lyckades!"), MB_ICONINFORMATION | MB_OK);
		}
	}
}


// CExportDataView message handlers

BOOL CExportDataView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}

		// Bring up export dialog when db connection has been set up
		if( m_pDB )
		{
			ShowExportDialog();
		}

		// Close this view so user can bring up this dialog again
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
	}

	return CFormView::OnCopyData(pWnd, pData);
}

int CExportDataView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CFormView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	return 0;
}



/////////////////////////////////////////////////////////////////////////////
// CExportDataFrame

IMPLEMENT_DYNCREATE(CExportDataFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CExportDataFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CExportDataFrame)
	/*ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_SHOWWINDOW()*/
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CExportDataFrame::CExportDataFrame()
{
}

CExportDataFrame::~CExportDataFrame()
{
}

void CExportDataFrame::ActivateFrame(int nCmdShow)
{
	// HACKHACK: hide this window
	CMDIChildWnd::ActivateFrame(SW_HIDE);
}

/*
int CExportDataFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_bFirstOpen = TRUE;

	return 0;
}

void CExportDataFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	// Save window position
	CString csBuf;
	csBuf.Format("%s\\%s", REG_ROOT, REGWP_MARKOPLANT_PLOTVIEWSELECT);
	SavePlacement(this, csBuf);

	CMDIChildWnd::OnClose();
}

BOOL CExportDataFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CExportDataFrame::OnSetFocus(CWnd *pOldWnd)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

void CExportDataFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	// Load window placement
	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	{
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format("%s\\%s", REG_ROOT, REGWP_MARKOPLANT_PLOTVIEWSELECT);
		LoadPlacement(this, csBuf);
	}
}
*/
