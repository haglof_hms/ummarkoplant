//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UMMarkoPlant.rc
//
#define IDD_FORMTEAM                    1100
#define IDD_FORMTRAKT                   1101
#define IDD_FORMPLOT                    1102
#define IDD_TEAMSELECT                  1103
#define IDD_TRAKTSELECT                 1104
#define IDD_PLOTSELECT                  1105
#define IDD_EXPORT                      1106
#define IDD_MESSAGE                     7131
#define IDC_HTML_TEXT                   7132
#define IDC_EDIT3                       7133
#define IDC_EDIT6                       7136
#define IDC_EDIT7                       7137
#define IDC_EDIT8                       7138
#define IDC_EDIT9                       7139
#define IDC_EDIT10                      7140
#define IDC_COMBO1                      7140
#define IDC_COMBO2                      7141
#define IDC_EDIT11                      7142
#define IDC_COMBO3                      7142
#define IDC_EDIT14                      7143
#define IDC_COMBO4                      7143
#define IDC_EDIT12                      7144
#define IDC_COMBO5                      7144
#define IDC_EDIT15                      7145
#define IDC_EDIT16                      7146
#define IDC_EDIT17                      7147
#define IDC_DATETIMEPICKER1             7147
#define IDC_EDIT18                      7148
#define IDC_EDIT19                      7149
#define IDC_EDIT20                      7150
#define IDC_COMBO30                     7150
#define IDC_COMBO6                      7151
#define IDC_COMBO7                      7152
#define IDC_COMBO8                      7153
#define IDC_COMBO9                      7154
#define IDC_COMBO10                     7155
#define IDC_COMBO11                     7156
#define IDC_COMBO12                     7157
#define IDC_EDIT22                      7158
#define IDC_COMBO13                     7159
#define IDC_COMBO14                     7160
#define IDC_COMBO15                     7161
#define IDC_COMBO16                     7162
#define IDC_COMBO17                     7163
#define IDC_EDIT28                      7163
#define IDC_COMBO18                     7164
#define IDC_EDIT26                      7164
#define IDC_COMBO19                     7165
#define IDC_EDIT27                      7165
#define IDC_COMBO20                     7166
#define IDC_COMBO21                     7167
#define IDC_COMBO22                     7168
#define IDC_EDIT29                      7169
#define IDC_EDIT30                      7170
#define IDC_COMBO25                     7171
#define IDC_EDIT31                      7171
#define IDC_COMBO26                     7172
#define IDC_COMBO23                     7172
#define IDC_EDIT23                      7173
#define IDC_COMBO24                     7173
#define IDC_EDIT24                      7174
#define IDC_EDIT25                      7175
#define IDC_COMBO27                     7176
#define IDC_COMBO28                     7177
#define IDC_BUTTON1                     7178
#define IDC_BUTTON2                     7179
#define IDC_EDIT1                       7179
#define IDC_EDIT32                      7180
#define IDC_EDIT2                       7180
#define IDC_COMBO29                     7181

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        7057
#define _APS_NEXT_COMMAND_VALUE         32817
#define _APS_NEXT_CONTROL_VALUE         7181
#define _APS_NEXT_SYMED_VALUE           7008
#endif
#endif
