#pragma once

class CDBFunc;


// CTeamSelectList dialog

class CTeamSelectList : public CFormView
{
	DECLARE_DYNCREATE(CTeamSelectList)

protected:
	CTeamSelectList();
	virtual ~CTeamSelectList();

public:
	void PopulateReport();
	bool SetupReport();

	virtual void OnInitialUpdate();

// Dialog Data
	enum { IDD = IDD_TEAMSELECT };

protected:
	CDBFunc *m_pDB;
	CMyReportCtrl m_report;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnReportItemClick(NMHDR *pNotifyStruct, LRESULT *pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
};




///////////////////////////////////////////////////////////////////////////////////////////
// CTeamSelectListFrame frame

class CTeamSelectListFrame : public CXTPFrameWndBase<CMDIChildWnd>
{
	DECLARE_DYNCREATE(CTeamSelectListFrame)

public:
	CTeamSelectListFrame();
	virtual ~CTeamSelectListFrame();

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	BOOL m_bFirstOpen;

	afx_msg void OnClose();
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	DECLARE_MESSAGE_MAP()
};
